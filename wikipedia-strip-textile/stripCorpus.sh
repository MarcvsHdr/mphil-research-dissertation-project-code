#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: $0 <TEXTILE_MARKUP_CORPUS_DIR>"
  exit 1
fi

for i in $1/*.textile
do
  newFileName=`echo $i | sed 's/textile$/txt/g'`
  if [ ! -f ${newFileName} ]
  then
    echo "Processing ${newFileName}..."
    ./strip-textile.rb <$i >${newFileName}
  else
    echo "Skipping ${newFileName}. It already exists..."
  fi
done
