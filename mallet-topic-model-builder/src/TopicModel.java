import cc.mallet.util.*;
import cc.mallet.types.*;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.*;
import cc.mallet.topics.*;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class TopicModel {

	// Constants for default parameters.
	static String DEFAULT_MALLET_INPUT_LOCATION = "/media/My Passport/bnc-selpref-counts/mallet_data_input_ncsubj.txt";
	static String DEFAULT_OUTPUT_TOPIC_LOCATION = "/media/My Passport/bnc-selpref-counts/mallet_topics_output_ncsubj.txt";
	//static String DEFAULT_MALLET_INPUT_LOCATION = "/home/marcvs/Desktop/working/Dissertation/Project/cached_data/mallet_data_input_small.txt";
	//static String DEFAULT_OUTPUT_TOPIC_LOCATION = "/home/marcvs/Desktop/working/Dissertation/Project/cached_data/mallet_topics_output_small.txt";
	static int DEFAULT_NUM_TOPICS = 100;
	static float DEFAULT_ALPHA_T = 0.01f;
	static float DEFAULT_BETA_W = 0.01f;
	static int DEFAULT_NUM_ITERATIONS = 50;
	
	// Actual parameters.
	static String malletInputLocation = DEFAULT_MALLET_INPUT_LOCATION;
	static String outputTopicLocation = DEFAULT_OUTPUT_TOPIC_LOCATION;
	static int numTopics = DEFAULT_NUM_TOPICS;
	static float alphaT = DEFAULT_ALPHA_T;
	static float betaW = DEFAULT_BETA_W;
	static int numIterations = DEFAULT_NUM_ITERATIONS;
	
	private static void parseArguments(String[] arguments) {
		try {
			if (arguments.length % 2 == 1) {
				throw new Exception();
			}
			for (int i = 0; i < arguments.length - 1; ++i) {
				if (arguments[i].equalsIgnoreCase("--input")) {
					malletInputLocation = arguments[i + 1];
					++i;
				} else if (arguments[i].equalsIgnoreCase("--output")) {
					outputTopicLocation = arguments[i + 1];
					++i;
				} else if (arguments[i].equalsIgnoreCase("--num-topics")) {
					numTopics = Integer.parseInt(arguments[i + 1]);
					++i;
				} else if (arguments[i].equalsIgnoreCase("--alpha-t")) {
					alphaT = Float.parseFloat(arguments[i + 1]);
					++i;
				} else if (arguments[i].equalsIgnoreCase("--beta-w")) {
					betaW = Float.parseFloat(arguments[i + 1]);
					++i;
				} else if (arguments[i].equalsIgnoreCase("--num-iterations")) {
					numIterations = Integer.parseInt(arguments[i + 1]);
					++i;
				}
			}
		} catch (Exception e) {
			System.err.println("Unrecognized command line arguments. Using debug defaults!");
			System.err.println("Arguments:");
			System.err.println("\t--input Path to the Mallet-format input file that contains the verbs and argument occurrences.");
			System.err.println("\t--output Path to the output file that will contain human-readable topic distributions.");
			System.err.println("\t--num-topics Number of latent variables for topics in the model.");
			System.err.println("\t--alpha-t Alpha parameter for topics.");
			System.err.println("\t--beta-w Beta parameter for topics.");
			System.err.println("\t--num-iterations Number of iterations for training to train on.");
		} finally {
			System.err.println("Running with the following parameters:");
			System.err.println("\t--input " + malletInputLocation);
			System.err.println("\t--output " + outputTopicLocation);
			System.err.println("\t--num-topics " + numTopics);
			System.err.println("\t--alpha-t " + alphaT);
			System.err.println("\t--beta-w " + betaW);
			System.err.println("\t--num-iterations " + numIterations);
		}
	}
	
	public static void writeSerializedDistribution(File outFile, ParallelTopicModel model, Alphabet dataAlphabet) throws IOException {
		// Open output writer.
		PrintWriter out = new PrintWriter(outFile);
		Formatter outBuffer;
		
		// Preface section in file: training parameters.
		out.println("# inputFile = " + malletInputLocation);
		out.println("# outputFile = " + outputTopicLocation);
		out.println("# numTopics = " + numTopics);
		out.println("# alphaT = " + alphaT);
		out.println("# betaW = " + betaW);
		out.println("# numIterations = " + numIterations);
		
		// First section in file: alpha parameter vector and beta parameter.
		outBuffer = new Formatter(new StringBuilder(), Locale.US);
		for (int i = 0; i < model.alpha.length; ++i) {
			outBuffer.format("%s%s", model.alpha[i], (i == model.alpha.length - 1) ? "\n" : " ");
		}
		outBuffer.format("%s", model.beta);
		out.println(outBuffer);
		
		// Second section in file: verb list.
		outBuffer = new Formatter(new StringBuilder(), Locale.US);
		for (int instance = 0; instance < model.getData().size(); ++instance) {
			outBuffer.format("%s%s", (instance > 0) ? " " : "", model.getData().get(instance).instance.getName());
		}
		out.println(outBuffer);
		
		// Third section in file: sparse matrix with topic distribution for each verb.
		for (int instance = 0; instance < model.getData().size(); ++instance) {
			outBuffer = new Formatter(new StringBuilder(), Locale.US);
			int[] topicCounts = new int[numTopics];
			Arrays.fill(topicCounts, 0);
			
			// Compute topic counts.
			int[] topicSequence = model.getData().get(instance).topicSequence.getFeatures();
			for (int token = 0; token < topicSequence.length; ++token) {
				topicCounts[topicSequence[token]]++;
			}
			
			// Print topic counts.
			for (int topic = 0; topic < numTopics; ++topic) {
				if (topicCounts[topic] > 0) {
					outBuffer.format("\t%3d %6d", topic, topicCounts[topic]);
				}
			}
			
			out.println(outBuffer);
		}
		
		// Fourth section in file: the argument list.
		outBuffer = new Formatter(new StringBuilder(), Locale.US);
		for (int argument = 0; argument < dataAlphabet.size(); ++argument) {
			outBuffer.format("%s%s", (argument > 0) ? " " : "", dataAlphabet.lookupObject(argument));
		}
		out.println(outBuffer);
		
		// Fifth section in file: topic distribution for each argument.
		// Compute topic counts.
		int[][] topicCounts = new int[dataAlphabet.size()][numTopics];
		for (int i = 0; i < dataAlphabet.size(); ++i) {
			Arrays.fill(topicCounts[i], 0);
		}
		ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
		for (int topic = 0; topic < numTopics; ++topic) {
			Iterator<IDSorter> it = topicSortedWords.get(topic).iterator();
			while (it.hasNext()) {
				IDSorter idSorter = it.next();
				topicCounts[idSorter.getID()][topic] += idSorter.getWeight();
			}
		}
		for (int argument = 0; argument < dataAlphabet.size(); ++argument) {
			outBuffer = new Formatter(new StringBuilder(), Locale.US);

			// Print topic counts.
			for (int topic = 0; topic < numTopics; ++topic) {
				if (topicCounts[argument][topic] > 0) {
					outBuffer.format("\t%3d %6d", topic, topicCounts[argument][topic]);
				}
			}
			
			out.println(outBuffer);		
		}
		
		// Cleanup.
		out.close();
	}

	public static InstanceList readInInstancesFromFile() throws IOException {
		// Create a pipe list through which to import the document.
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
		pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
		pipeList.add(new TokenSequence2FeatureSequence());

		// Create the instance list based on the above processing pipeline.
		InstanceList instances = new InstanceList (new SerialPipes(pipeList));

		// Read in the data.
		File inputFile;
		inputFile = new File(malletInputLocation);
		Reader fileReader = new InputStreamReader(new FileInputStream(inputFile), "UTF-8");
		// verb <tab> label <tab> data
		Pattern malletInputDataLine = Pattern.compile("^(\\S*)[\\s\\t]*(\\S*)[\\s\\t]*(.*)$");
		instances.addThruPipe(new CsvIterator (fileReader, malletInputDataLine, 3, 2, 1)); // data, label, verb
		
		// Return the newly created instance list.
		return instances;
	}
	
	public static ParallelTopicModel trainTopicModel(InstanceList instances) throws IOException {
		ParallelTopicModel model = new ParallelTopicModel(numTopics, numTopics * alphaT, betaW);
		model.addInstances(instances);
		model.setNumThreads(2);
		model.setNumIterations(numIterations);
		model.estimate();
		return model;
	}
	
	public static void main(String[] args) throws Exception {
		// Parse parameters.
		parseArguments(args);
		
		// Load up the instances from a file.
		InstanceList instances = readInInstancesFromFile();

		// Train a topic model based on those instances.
		ParallelTopicModel model = trainTopicModel(instances);
		
		// The data alphabet maps word IDs to strings.
		Alphabet dataAlphabet = instances.getDataAlphabet();
		
		// Serialize the topic model to the output file.
		writeSerializedDistribution(new File(outputTopicLocation), model, dataAlphabet);
	}
}
