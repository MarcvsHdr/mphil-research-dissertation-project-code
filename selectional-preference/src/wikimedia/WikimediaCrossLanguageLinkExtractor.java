package wikimedia;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import selpref.data.DependencyParsedSentence;

public class WikimediaCrossLanguageLinkExtractor extends DefaultHandler {

  // Translation information.
  String sourceLanguage;
  Set<String> targetLanguages;
  // Maps from source language word to
  // (target language, target language word)
  TreeMap<String, TreeMap<String, String>> translations;
  Integer articlesProcessed = 0;

  static public TreeMap<String, String> languageNames;

  static {
    languageNames = new TreeMap<String, String>();
    languageNames.put("en", "English");
    languageNames.put("de", "German");
    languageNames.put("es", "Spanish");
    languageNames.put("ro", "Romanian");
  }

  enum ParseState {
    IN_PAGE,
    IN_PAGE_TITLE,
    IN_PAGE_TEXT,
    OUTSIDE
  }

  // Default handler states.
  ParseState currentState = ParseState.OUTSIDE;
  String currentTitle;
  StringBuffer currentText;

  public void startElement(
      String uri,
      String localName,
      String qName,
      Attributes attributes) throws SAXException {
    // Listening for <page> tag.
    if (qName.equalsIgnoreCase("page")) {
      if (currentState == ParseState.OUTSIDE) {
        currentState = ParseState.IN_PAGE;
        currentTitle = null;
        currentText = new StringBuffer();
      } else {
        throw new SAXException("Found <page> inside <page> tag.");
      }
      return;
    }

    // Listening for <title> tag.
    if (qName.equalsIgnoreCase("title")) {
      if (currentState == ParseState.IN_PAGE) {
        currentState = ParseState.IN_PAGE_TITLE;
      } else {
        throw new SAXException("Found <title> not inside <page> tag.");
      }
      return;
    }

    // Listening for <text> tag.
    if (qName.equalsIgnoreCase("text")) {
      if (currentState == ParseState.IN_PAGE) {
        currentState = ParseState.IN_PAGE_TEXT;
      } else {
        throw new SAXException("Found <text> not inside <page> tag.");
      }
      return;
    }
  }

  public void endElement(
      String uri,
      String localName,
      String qName) throws SAXException {
    // Listening for </page> tag.
    if (qName.equalsIgnoreCase("page")) {
      if (currentState == ParseState.IN_PAGE) {
        // Resetting the current title, so that we don't consider it again.
        currentState = ParseState.OUTSIDE;
        currentTitle = null;
        currentText = new StringBuffer();
      } else {
        throw new SAXException("Found </page> not in <page> tag.");
      }
      this.articlesProcessed = this.articlesProcessed + 1;
      if (this.articlesProcessed % 1000 == 0) {
        System.err.println("So far parsed " +
                           this.articlesProcessed + " articles...");
      }
      return;
    }

    // Listening for </title> tag.
    if (qName.equalsIgnoreCase("title")) {
      if (currentState == ParseState.IN_PAGE_TITLE) {
        currentState = ParseState.IN_PAGE;
      } else {
        throw new SAXException("Found </title> not inside </title> tag.");
      }
      return;
    }

    // Listening for </text> tag.
    if (qName.equalsIgnoreCase("text")) {
      if (currentState == ParseState.IN_PAGE_TEXT) {
        currentState = ParseState.IN_PAGE;
      } else {
        throw new SAXException("Found </text> not inside </text> tag.");
      }
      if (this.currentTitle != null) {
        // Give this page a chance for finding translations in text.
        lookForTranslationsInText();
      }
      return;
    }
  }

  public void characters(char ch[], int start, int length)
      throws SAXException {
        if (currentState == ParseState.IN_PAGE_TITLE) {
          // Remember the page title.
          String title = new String(ch, start, length);
          if (title.matches(DependencyParsedSentence.lexemRegex)) {
            currentTitle = title;
          }
          return;
        } else if (currentState == ParseState.IN_PAGE_TEXT) {
          // Add to the page text.
          this.currentText.append(ch, start, length);
        }
      }

  private void lookForTranslationsInText() {
    // Look for translation candidates.
    String currentPosTag = null;
    Pattern posTagPattern = Pattern.compile("==*(Noun|Verb)==*");
    Pattern translationsPattern = Pattern.compile("==*Translations==*");
    TreeMap<String, String> titleTranslations = null;
    Boolean scansTranslations = false;

    String[] textLines = this.currentText.toString().split("\n");

    for (int i = 0; i < textLines.length; ++i) {

      // If we hit a line specifying the POS tag for a noun or
      // a verb, then update current state and skip to the next line.
      Matcher posmatcher = posTagPattern.matcher(textLines[i]);
      if (posmatcher.find()) {
        String matcherPosTag = posmatcher.group(1);
        if (matcherPosTag.equalsIgnoreCase("Noun")) {
          currentPosTag = "--N";
        } else if (matcherPosTag.equalsIgnoreCase("Verb")) {
          currentPosTag = "--V";
        } else {
          currentPosTag = null;
        }
        scansTranslations = false;
        continue;
      }

      // If we hit a line specifying the beginning of the
      // translations list, start listening.
      Matcher translationsmatcher = translationsPattern.matcher(textLines[i]);
      if (translationsmatcher.find() && currentPosTag != null) {
        scansTranslations = true;
        continue;
      }

      if (scansTranslations) {
        if (textLines[i].startsWith("{{trans-top|")) {
          titleTranslations = new TreeMap<String, String>();
        } else if (textLines[i].startsWith("{{trans-bottom}}")) {
          if (titleTranslations != null) {
            if (titleTranslations.size() == this.targetLanguages.size()) {
              this.translations.put(
                  currentTitle + currentPosTag, titleTranslations);
            }
          }
          titleTranslations = null;
        } else if (titleTranslations != null) {
          // Look for a particular translation.
          for (String language : this.targetLanguages) {
            Pattern translation = Pattern.compile(
                "\\* " + languageNames.get(language) + ": \\{\\{(t.\\|.*?)\\}\\}");
            Matcher matcher = translation.matcher(textLines[i]);
            if (matcher.find()) {
              String[] translationTokens = matcher.group(1).split("\\|");
              if (translationTokens.length >= 3) {
                String actualTranslation = translationTokens[2];
                if (actualTranslation.matches(
                        DependencyParsedSentence.lexemRegex)) {
                  titleTranslations.put(
                      language, actualTranslation + currentPosTag);
                }
              }
            }
          }
        }
      }
    }
  }

  private void extractTranslationsFromWikipediaDump(File wikipediaDump)
      throws Exception {
        // Create the parser.
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        // And parse.
        saxParser.parse(wikipediaDump, this);
      }

  public WikimediaCrossLanguageLinkExtractor(
      String sourceLanguage,
      Set<String> targetLanguages,
      File wikimediaDump) throws Exception {
    this.sourceLanguage = sourceLanguage;
    this.targetLanguages = targetLanguages;
    this.translations = new TreeMap<String, TreeMap<String, String>>();
    this.extractTranslationsFromWikipediaDump(wikimediaDump);
  }

  public void writeToPrintStream(PrintStream out) {
    StringBuffer line = new StringBuffer();

    // Output Languages.
    line.append(sourceLanguage);
    for (String language : this.targetLanguages) {
      line.append(" " + language);
    }
    out.println(line);

    // Output translation tuples (in original casing!).
    for (String word : this.translations.keySet()) {
      line = new StringBuffer();
      line.append(word);
      TreeMap<String, String> translations =
          this.translations.get(word);
      for (String language : translations.keySet()) {
        line.append(" " + translations.get(language));
      }
      out.println(line);
    }
  }

  public void writeToFile(File outputFile) throws Exception {
    // Check that the extension of the file is .translations
    if (!outputFile.getName().endsWith(".translations")) {
      throw new Exception(
          "Error when trying to write Translations to file '" +
          outputFile.getName() +
          "'. Expected extension '.translations'"
          );
    } else {
      System.err.println("Writing Translations to file'" +
                         outputFile.getName() + "'...");
    }

    // Try creating a PrintStream from the file and writing there.
    PrintStream out = new PrintStream(outputFile);
    writeToPrintStream(out);
    out.close();

    System.err.println("\tDone!");
  }
}
