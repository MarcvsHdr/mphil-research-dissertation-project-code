package utils;

import java.util.*;

public class VectorUtils {

  public static Double norm(Vector<Double> vector) {
    Double sum = 0.0D;
    for (Double elem : vector) {
      sum += elem * elem;
    }
    return Math.sqrt(sum);
  }

  public static Vector<Double> normalize(Vector<Double> vector)
      throws Exception {
        Vector<Double> returnValue = new Vector<Double>();
        Double vectorNorm = VectorUtils.norm(vector);
        if (vectorNorm == 0.0D) {
          throw new Exception("Vector is {0}.");
        }
        for (Double elem : vector) {
          returnValue.add(elem / vectorNorm);
        }
        return returnValue;
      }

  public static Double computeEuclideanDistanceOnNormalizedVectors(
      Vector<Double> left,
      Vector<Double> right) throws Exception {
    if (left.size() != right.size()) {
      throw new Exception("Vectors of unequal size.");
    }

    if (left.size() == 0) {
      throw new Exception("Vectors of lenght zero.");
    }

    Double totalDistance = 0.0D;
    for (int i = 0; i < left.size(); ++i) {
      totalDistance += (left.elementAt(i) - right.elementAt(i)) *
          (left.elementAt(i) - right.elementAt(i));
    }
    return Math.sqrt(totalDistance);
  }

  public static Double computeEuclideanDistanceOnUnnormalizedVectors(
      Vector<Double> left,
      Vector<Double> right) throws Exception {
    if (left.size() != right.size()) {
      throw new Exception("Vectors of unequal size.");
    }

    if (left.size() == 0) {
      throw new Exception("Vectors of lenght zero.");
    }

    try {
      left = normalize(left);
      right = normalize(right);
    } catch (Exception e) {
      // This means one of the vectors is "0.0D".
      // Distance is the largest possible in the space.
      return new Double(left.size());
    }

    Double totalDistance = 0.0D;
    for (int i = 0; i < left.size(); ++i) {
      totalDistance += (left.elementAt(i) - right.elementAt(i)) *
          (left.elementAt(i) - right.elementAt(i));
    }
    return Math.sqrt(totalDistance);
  }

  public static Double cosineMetric(
      Vector<Double> left,
      Vector<Double> right) throws Exception {
    if (left.size() != right.size()) {
      throw new Exception("Error computing cosine metrics." +
                          "Vectors have different lengths.");
    }

    Double dotProduct = 0.0D;
    for (int i = 0; i < left.size(); ++i) {
      dotProduct += left.elementAt(i) * right.elementAt(i);
    }
    if (dotProduct == 0.0D) {
      return dotProduct;
    } else {
      return dotProduct / (norm(left) * norm(right));
    }
  }

  public static Double computePearsonR(
      Vector<Double> left,
      Vector<Double> right) throws Exception {
    if (left.size() != right.size()) {
      throw new Exception("Vectors of unequal size.");
    }

    if (left.size() == 0) {
      throw new Exception("Vectors have zero length.");
    }

    // Compute averages.
    Double averageLeft = 0.0D;
    Double averageRight = 0.0D;
    for (int i = 0; i < left.size(); ++i) {
      averageLeft += left.elementAt(i);
      averageRight += right.elementAt(i);
    }
    averageLeft /= left.size();
    averageRight /= right.size();

    // Compute covariance and standard deviations.
    Double covariance = 0.0D;
    Double stdLeft = 0.0D;
    Double stdRight = 0.0D;
    for (int i = 0; i < left.size(); ++i) {
      Double difLeft = left.elementAt(i) - averageLeft;
      Double difRight = right.elementAt(i) - averageRight;
      covariance += difLeft * difRight;
      stdLeft += difLeft * difLeft;
      stdRight += difRight * difRight;
    }

    return covariance / (Math.sqrt(stdLeft) * Math.sqrt(stdRight));
  }

  public static Double computeSpearmanRho(
      Vector<Double> left,
      Vector<Double> right) throws Exception {
    if (left.size() != right.size()) {
      throw new Exception("Vectors of unequal size.");
    }

    if (left.size() == 0) {
      throw new Exception("Vectors have zero length.");
    }

    Vector<Double> ranksLeft = convertToRanks(left);
    Vector<Double> ranksRight = convertToRanks(right); 
    return computePearsonR(ranksLeft, ranksRight);
  }

  private static Vector<Double> convertToRanks(Vector<Double> values) {
    // Define a tuple class.
    class SpearmanTriple {
      Integer originalIndex = Integer.MAX_VALUE;
      Double value = 0.0D;
      Double rank = Double.NaN;

      public SpearmanTriple(Integer originalIndex, Double value) {
        this.originalIndex = originalIndex;
        this.value = value;
      }
    }

    // Create a vector of triples.
    Vector<SpearmanTriple> triples = new Vector<SpearmanTriple>();
    for (int i = 0; i < values.size(); ++i) {
      triples.add(new SpearmanTriple(i, values.elementAt(i)));
    }

    // Sort the input vector according to value
    Collections.sort(triples, new Comparator<SpearmanTriple>() {
                     public int compare(SpearmanTriple left,
                                        SpearmanTriple right) {
                     return left.value.compareTo(right.value);
                     }
                     });

    // Begin assigning ranks.
    Integer i = 0;
    Integer next_i = 0;
    while (i < triples.size()) {
      // Skip over identical values.
      while (next_i < triples.size() &&
             triples.elementAt(next_i).value.compareTo(
                 triples.elementAt(i).value) == 0) {
        next_i++;
      }

      // Assign the average ranks.
      Double average_rank = i + (next_i - i - 1) / 2.0D;
      while (i < next_i) {
        triples.elementAt(i).rank = average_rank;
        i++;
      }
    }

    // Sort again based on the original index.
    Collections.sort(triples, new Comparator<SpearmanTriple>() {
                     public int compare(SpearmanTriple left,
                                        SpearmanTriple right) {
                     return left.originalIndex.compareTo(right.originalIndex);
                     }
                     });

    // Extract and return the ranks.
    Vector<Double> ranks = new Vector<Double>();
    for (int triple_index = 0;
         triple_index < triples.size();
         ++triple_index) {
      ranks.add(triples.elementAt(triple_index).rank);
    }
    return ranks;
  }

  public static String toString(Vector<Double> vector) {
    StringBuffer out = new StringBuffer();
    out.append("{");
    for (int i = 0; i < vector.size(); ++i) {
      out.append((i == 0 ? "" : ", ") + vector.elementAt(i));
    }
    return out.toString();
  }

  public static String toString(String vectorName, Vector<Double> vector) {
    return vectorName + ": " + toString(vector);
  }
}
