package utils;

import java.util.*;

public class GlobalParameters {

  TreeMap<String, Parameter> parameters;

  public enum CommandLinePresence {
    FORBIDDEN,
        MANDATORY,
        OPTIONAL
  }

  public GlobalParameters() {
    this.parameters = new TreeMap<String, Parameter>();
  }

  public class Parameter {
    String name;
    Object value;
    Object defaultValue;
    String description;
    CommandLinePresence commandLinePresence;

    public Parameter(
        String name,
        Object defaultValue,
        String description,
        CommandLinePresence commandLinePresence) throws Exception {
      this.name = name;
      this.defaultValue = defaultValue;
      this.description = description;
      this.commandLinePresence = commandLinePresence;
      if (this.commandLinePresence == CommandLinePresence.FORBIDDEN &&
          this.defaultValue == null) {
        throw new Exception ("Error. Argument '" + name +
                             "' is forbidden on the command line, but has " +
                             "no default value.");
      }
    }
  }

  public void addParameter(Parameter parameter) {
    this.parameters.put(parameter.name, parameter);
  }

  private Object getParameter(String parameterName) throws Exception {
    if (!this.parameters.containsKey(parameterName)) {
      throw new Exception("Parameter '" + parameterName +
                          "' is not a valid parameter.");
    }
    Parameter parameter = this.parameters.get(parameterName);
    if (parameter.value == null) {
      return parameter.defaultValue;
    } else {
      return parameter.value;
    }
  }

  public String getString(String parameterName) throws Exception {
    return (String)getParameter(parameterName);
  }

  public Integer getInteger(String parameterName) throws Exception {
    return Integer.parseInt((String)getParameter(parameterName));
  }

  public Double getDouble(String parameterName) throws Exception {
    return Double.parseDouble((String)getParameter(parameterName));
  }

  public Boolean getBoolean(String parameterName) throws Exception {
    return Boolean.parseBoolean((String)getParameter(parameterName));
  }

  public void parseCommandLineArguments(String arguments[])
      throws Exception {
        // Arguments should always be an even number.
        if (arguments.length % 2 != 0) {
          System.err.println("Error parsing arguments. " +
                             "Should have even number of arguments.");
        } else {
          for (int i = 0; i < arguments.length; i += 2) {
            String argName = arguments[i];
            String argVal = arguments[i + 1];
            if (!argName.startsWith("--")) {
              throw new Exception ("Error! " +
                                   "Parameter names must start with '--'");
            }
            argName = argName.substring(2);
            if (!this.parameters.containsKey(argName)) {
              throw new Exception(
                  "Unrecognized parameter '" + argName + "'.");
            } else {
              Parameter parameter = this.parameters.get(argName);
              if (parameter.commandLinePresence ==
                  CommandLinePresence.FORBIDDEN) {
                throw new Exception ("Erorr. Parameter '" + argName +
                                     "' is forbidden on the command line.");
              }
              parameter.value = argVal;
            }
          }
        }

        // Make sure that all mandatory arguments were supplied.
        for (String parameterName : this.parameters.keySet()) {
          Parameter parameter = this.parameters.get(parameterName);
          if (parameter.commandLinePresence == CommandLinePresence.MANDATORY &&
              parameter.value == null) {
            throw new Exception("Error! Argument '" + parameterName + "' is " +
                                "mandatory, but not specified " +
                                "on the command line.");
          }
        }

        // Finally, dump all parameters.
        dumpParameters();
      }

  public void printUsage() {
    System.err.println("Usage:");
    for (String parameterName : this.parameters.keySet()) {
      Parameter parameter = this.parameters.get(parameterName);
      System.err.println("\t--" + parameterName +
                         (parameter.defaultValue != null ?
                          ("[default = '" +
                           parameter.defaultValue.toString() + "']") :
                          "") +
                         parameter.description);
    }
  }

  public void dumpParameters() {
    System.err.println("Running with the following parameters: ");
    for (String parameterName : this.parameters.keySet()) {
      try {
        System.err.println("\t--" + parameterName + " == " +
                           getParameter(parameterName));
      } catch (Exception e) {
        // This should never happen.
      }
    }
  }
}
