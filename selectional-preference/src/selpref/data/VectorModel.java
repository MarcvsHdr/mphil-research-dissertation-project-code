package selpref.data;

import java.io.*;
import java.util.*;
import utils.VectorUtils;

public abstract class VectorModel implements Serializable {

  private static final long serialVersionUID = -3221161535691079390L;

  OccurrenceModel occurrenceModel;
  Vector<String> dimensions;
  TreeMap<String, Vector<Double>> vectors;
  private Boolean cachingEnabled = false;

  protected void swapData(VectorModel other) {
    {
      OccurrenceModel aux = this.occurrenceModel;
      this.occurrenceModel = other.occurrenceModel;
      other.occurrenceModel = aux;
    }
    {
      Vector<String> aux = this.dimensions;
      this.dimensions = other.dimensions;
      other.dimensions = aux;
    }
    {
      TreeMap<String, Vector<Double>> aux = this.vectors;
      this.vectors = other.vectors;
      other.vectors = aux;
    }
    {
      Boolean aux = this.cachingEnabled;
      this.cachingEnabled = other.cachingEnabled;
      other.cachingEnabled = aux;
    }
  }

  protected VectorModel() {
    this.occurrenceModel = new OccurrenceModel();
    this.dimensions = new Vector<String>();
    this.vectors = new TreeMap<String, Vector<Double>>();
  }

  public VectorModel(File inputFile) throws Exception {
    this();
    this.parseFromFile(inputFile);
  }

  /** Assigns a new set of dimensions to the vector model.
   * Be careful when using this, as it may break the consistency of the
   * vector produced within the model.
   * @param dimensions The new vector of dimensions of the model.
   */
  public void setDimensions(Vector<String> dimensions) {
    this.dimensions = dimensions;
  }

  public Set<String> getKeys() {
    return occurrenceModel.getKeys();
  }

  public Vector<String> getMostFrequentKeys(Integer n, String suffix)
      throws Exception {
    return occurrenceModel.getMostFrequentKeys(n, suffix);
  }

  protected Double getVectorValue(String key, String dimension)
      throws Exception {
        throw new Exception (
            "Not implemented in generic class VectorModel.");
      }

  public Double getSimilarity(String keyLeft, String keyRight)
      throws Exception {
        Vector<Double> leftVector = this.getVector(keyLeft);
        Vector<Double> rightVector = this.getVector(keyRight);
        return VectorUtils.cosineMetric(leftVector, rightVector);
      }

  /** Gets the vector for a key, based on an occurrence model. */
  public Vector<Double> getVector(String key) throws Exception {
    if (vectors.containsKey(key)) {
      return this.vectors.get(key);
    } else {
      // Build the vector for the key.
      Vector<Double> coordinates = new Vector<Double>();
      for (int i = 0; i < this.dimensions.size(); ++i) {
        try {
          coordinates.add(getVectorValue(key,
                                         this.dimensions.elementAt(i)));
        } catch (Exception e) {
          coordinates.add(0.0D);
        }
      }

      // Add the key to the vector model.
      if (cachingEnabled) {
        vectors.put(key, coordinates);
      }
      return coordinates;
    }
  }

  /** Write a vector model to a print stream. */
  public void writeToPrintStream(PrintStream out) throws Exception {
    // Print out a few comment lines with statistics.
    out.println("# Number of dimensions: " + this.dimensions.size());

    // Print the actual dimensions, separated by spaces.
    Boolean firstDimension = true;
    StringBuffer dimensionLine = new StringBuffer();
    for (String dimension : this.dimensions) {
      dimensionLine.append(
          (firstDimension == true ? "" : " ") + dimension);
      firstDimension = false;
    }
    out.println(dimensionLine);

    // Print out the associated occurrence model.
    this.occurrenceModel.writeToPrintStream(out);
  }

  /** Loads a vector model from a buffered reader. */
  private void parseFromBufferedReader(BufferedReader in) throws Exception {
    String line;
    while ((line = in.readLine()) != null) {
      if (line.startsWith("#")) {
        // Skip comment lines.
        continue;
      } else {
        // Until you've reached a dimensions line.
        String[] parsed_dimensions = line.split("[ \\t]+");
        dimensions = new Vector<String>();
        for (int i = 0; i < parsed_dimensions.length; ++i) {
          if (parsed_dimensions[i].length() > 0) {
            dimensions.add(parsed_dimensions[i]);
          }
        }
        break;
      }
    }
    if (line == null) {
      throw new Error("Error! No dimensions found in vector model.");
    } else {
      this.occurrenceModel.parseFromBufferedReader(in);
    }
  }

  /** Loads a vector model from a .vector_model file. */
  private void parseFromFile(File inputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!inputFile.getName().endsWith(".vector_model")  &&
        !inputFile.getName().endsWith(".bin_vector_model")) {
      throw new Exception(
          "Error when trying to load VectorModel from file '" +
          inputFile.getName() +
          "'. Expected extension '.vector_model' or " +
          "'.bin_vector_model'"
          );
    } else {
      System.err.println("Loading VectorModel from file '"
                         + inputFile.getName() + "'...");
    }

    if (inputFile.getName().endsWith(".vector_model")) {
      // Read in object as plain text.
      BufferedReader in = new BufferedReader(new FileReader(inputFile));
      parseFromBufferedReader(in);			
      in.close();
    } else {
      // Read in object as binary.
      ObjectInputStream in =
          new ObjectInputStream(
              new FileInputStream(inputFile));
      this.swapData((VectorModel)in.readObject());
      in.close();
    }

    System.err.println("\tDone!");
  }

  /** Saves a vector model to a .vector_model file. */
  public void writeToFile(File outputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!outputFile.getName().endsWith(".vector_model") &&
        !outputFile.getName().endsWith(".bin_vector_model")) {
      throw new Exception(
          "Error when trying to write VectorModel to file '" +
          outputFile.getName() +
          "'. Expected extension '.vector_model' or " +
          "'.bin_vector_model'"
          );
    } else {
      System.err.println("Writing VectorModel to file'" +
                         outputFile.getName() + "'...");
    }

    if (outputFile.getName().endsWith(".vector_model")) {
      // Writing object in plain text form.
      PrintStream out = new PrintStream(outputFile);
      writeToPrintStream(out);
      out.close();
    } else {
      // Writing object in binary form.
      ObjectOutputStream out =
          new ObjectOutputStream(
              new FileOutputStream(outputFile));
      out.writeObject(this);
      out.close();
    }

    System.err.println("\tDone!");
  }
}
