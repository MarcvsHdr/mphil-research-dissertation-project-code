package selpref.data;

import utils.GlobalParameters;
import utils.GlobalParameters.CommandLinePresence;

public class PlausibilityEstimatorParameters {

  public static GlobalParameters de_dobj;
  public static GlobalParameters de_wiki_dobj;
  public static GlobalParameters de_dobj_cov;
  public static GlobalParameters de_ncsubj;
  public static GlobalParameters de_wiki_ncsubj;
  public static GlobalParameters de_ncsubj_cov;
  public static GlobalParameters en_dobj;
  public static GlobalParameters en_ncsubj;
  public static GlobalParameters en_dobj_kl;
  public static GlobalParameters es_dobj;
  public static GlobalParameters es_wiki_dobj;
  public static GlobalParameters es_dobj_cov;
  public static GlobalParameters es_ncsubj;
  public static GlobalParameters es_wiki_ncsubj;
  public static GlobalParameters es_ncsubj_cov;
  public static GlobalParameters ro_dobj;
  public static GlobalParameters ro_ncsubj;

  static {
    try {
      {
        // English Direct Objects
        en_dobj = new GlobalParameters();
        en_dobj.addParameter(en_dobj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/pado_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "golden-standard-language",
                "en",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "target-transfer-language",
                "de",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/pado_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        en_dobj.addParameter(en_dobj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // English Direct Objects Keller & Lapata
        en_dobj_kl = new GlobalParameters();
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/kellerlapata_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "golden-standard-language",
                "en",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "target-transfer-language",
                "de",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/kellerlapata_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        en_dobj_kl.addParameter(en_dobj_kl.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // English Subjects
        en_ncsubj = new GlobalParameters();
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/pado_plausibility_ncsubj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "golden-standard-language",
                "en",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "target-transfer-language",
                "de",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/pado_plausibility_ncsubj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        en_ncsubj.addParameter(en_ncsubj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // German Direct Objects
        de_dobj = new GlobalParameters();
        de_dobj.addParameter(de_dobj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "golden-standard-language",
                "de",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        de_dobj.addParameter(de_dobj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // German Direct Objects with Coverage
        de_dobj_cov = new GlobalParameters();
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_dobj_cov.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "golden-standard-language",
                "de",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_dobj_cov_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        de_dobj_cov.addParameter(de_dobj_cov.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Direct Objects
        de_wiki_dobj = new GlobalParameters();
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wiki_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "golden-standard-language",
                "de",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wiki_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        de_wiki_dobj.addParameter(de_wiki_dobj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // German Subjects
        de_ncsubj = new GlobalParameters();
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_ncsubj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "golden-standard-language",
                "de",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_ncsubj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj.addParameter(de_ncsubj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // German Subjects with Coverage
        de_ncsubj_cov = new GlobalParameters();
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_ncsubj_cov.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "golden-standard-language",
                "de",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_ncsubj_cov_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        de_ncsubj_cov.addParameter(de_ncsubj_cov.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Subjects
        de_wiki_ncsubj = new GlobalParameters();
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wiki_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de-en_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_ncsubj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "golden-standard-language",
                "de",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wiki_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/brockmann_plausibility_ncsubj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        de_wiki_ncsubj.addParameter(de_wiki_ncsubj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Direct Objects
        es_dobj = new GlobalParameters();
        es_dobj.addParameter(es_dobj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-es_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "golden-standard-language",
                "es",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        es_dobj.addParameter(es_dobj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Direct Objects
        es_wiki_dobj = new GlobalParameters();
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_wiki_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-es_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "golden-standard-language",
                "es",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_wiki_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        es_wiki_dobj.addParameter(es_wiki_dobj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Direct Objects with Coverage
        es_dobj_cov = new GlobalParameters();
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-es_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_dobj_cov.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "golden-standard-language",
                "es",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_dobj_cov_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        es_dobj_cov.addParameter(es_dobj_cov.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Subjects
        es_ncsubj = new GlobalParameters();
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-es_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_ncsubj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "golden-standard-language",
                "es",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_ncsubj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj.addParameter(es_ncsubj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Subjects At Full Coverage
        es_ncsubj_cov = new GlobalParameters();
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-es_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_ncsubj_cov.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "golden-standard-language",
                "es",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_ncsubj_cov_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        es_ncsubj_cov.addParameter(es_ncsubj_cov.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Spanish Subjects
        es_wiki_ncsubj = new GlobalParameters();
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_wiki_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-es_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_ncsubj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "golden-standard-language",
                "es",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_wiki_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/padoes_plausibility_ncsubj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        es_wiki_ncsubj.addParameter(es_wiki_ncsubj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Romanian Direct Objects
        ro_dobj = new GlobalParameters();
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/ro_dobj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/ro.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(es_dobj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-ro_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/scoica_plausibility_dobj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "golden-standard-language",
                "ro",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/ro_dobj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_dobj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/scoica_plausibility_dobj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        ro_dobj.addParameter(ro_dobj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
      {
        // Romanian Subjects
        ro_ncsubj = new GlobalParameters();
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "occurrence-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/ro_ncsubj.occurrences",
                "Path to human-readable verb-argument occurrences.",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/ro.vector_model",
                "Path to noun vector model (for similarity computation).",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "multilingual-vector-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-ro_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
                "Path to multilingual vector model (for neighbour selection, for smoothing).",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "golden-standard",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/scoica_plausibility_ncsubj.golden_standard",
                "Path to the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "golden-standard-language",
                "ro",
                "The language of the plausibility test set.",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "golden-standard-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/ro_ncsubj.topic_model",
                "Path to human-readable LDA topic distribution.",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "target-transfer-language",
                "en",
                "The languages from which we will transfer probabilities",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "target-transfer-language-topic-model",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_ncsubj.topic_model",
                "The topic models for the languages from which we will transfer probabilities.",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "csv-output",
                "/home/marcvs/Desktop/working/Dissertation/Project/Data/scoica_plausibility_ncsubj_sxs.csv",
                "Where the results will be written.",
                CommandLinePresence.OPTIONAL));
        ro_ncsubj.addParameter(ro_ncsubj.new Parameter(
                "log-transform",
                "false",
                "Whether all plausibilities are to be log-transformed.",
                CommandLinePresence.OPTIONAL));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
