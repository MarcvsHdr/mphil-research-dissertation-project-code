package selpref.data;

import java.util.Vector;

/** A dependency-parsed sentence is a linearized tree-structure. */
public class DependencyParsedSentence extends TaggedSentence {

  public final static String lexemRegex = "^[\\p{L}-]*$";

  private class Relation {
    Integer head;
    Integer dependent;
    String type;

    public Relation(Integer head, Integer dependent, String type) {
      this.head = head;
      this.dependent = dependent;
      this.type = type;
    }
  }

  // Sentence relations.
  protected Vector<Relation> relations;

  protected void clear() {
    super.clear();
    relations = new Vector<Relation>();
  }

  // The constructor of a sentence.
  public DependencyParsedSentence() {
    this.clear();
  }

  /** Adds a grammatical relation to the sentence. */
  public void addRelation(Integer head, Integer dependent, String type) {
    relations.add(new Relation(head, dependent, type));
  }

  /** Gets the list of well-formed grammatical relations in the sentence. */
  public Vector<GrammaticalRelation> getGrammaticalRelations() {
    Vector<GrammaticalRelation> returnValue =
        new Vector<GrammaticalRelation>();
    for (Relation relation : this.relations) {
      // If we're filtering by head POS.
      Word head = null;
      if (!words.containsKey(relation.head)) {
        continue;
      } else {
        head = words.get(relation.head);
      }

      // If we're filtering by dependent POS.
      Word dependent = null;
      if (!words.containsKey(relation.dependent)) {
        continue;
      } else {
        dependent = words.get(relation.dependent);
      }

      // Count this grammatical relation.
      returnValue.add(new GrammaticalRelation(
              head.lemma, head.pos, head.extended_pos,
              dependent.lemma, dependent.pos, dependent.extended_pos,
              relation.type));
    }

    // And return the relations.
    return returnValue;
  }

  /** Counts the relations in the sentence (possibly filtering by POS and
   * relation type) to an occurrence model.
   *
   * @param occurrenceModel
   * @param headPos Leave null for any head POS.
   * @param dependentPos Leave null for any dependent POS.
   * @param dependentExtendedPos Leave null for any dependent extended POS.
   * @param relationType Leave null for any relation type.
   * @param secondary_key This will be the secondary key in the occurrence
   * model.
   */
  public void countRelationsToOccurrenceModel(
      OccurrenceModel occurrenceModel,
      String headPos,
      String dependentPos,
      String dependentExtendedPos,
      String relationType,
      String secondary_key) {
    for (GrammaticalRelation gr: this.getGrammaticalRelations()) {
      // If we're filtering by relation type.
      if (relationType != null &&
          !gr.type.equalsIgnoreCase(relationType)) {
        continue;
      }

      // If we're filtering by head POS.
      if (headPos != null &&
          !gr.headPos.equalsIgnoreCase(headPos)) {
        continue;
      }

      // If we're filtering by dependent POS.
      if (dependentPos != null &&
          !gr.dependentPos.equalsIgnoreCase(dependentPos)) {
        continue;
      }

      // If we're filtering by dependent extended POS.
      if (dependentExtendedPos != null &&
          !gr.dependendExtendedPos.equalsIgnoreCase(dependentExtendedPos)) {
        continue;
      }

      // Finally, we only retain lemmas made up of letters.
      if (gr.headLemma.matches(lexemRegex) &&
          gr.dependentLemma.matches(lexemRegex)) {
        occurrenceModel.addTriple(
            gr.headLemma, secondary_key, gr.dependentLemma);
      }
    }
  }
}
