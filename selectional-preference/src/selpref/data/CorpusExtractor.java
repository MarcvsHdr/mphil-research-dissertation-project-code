package selpref.data;

import java.io.*;
import java.util.Vector;

public class CorpusExtractor {

  // The parser which will be called back on the files.
  private CorpusFileParser corpusFileParser;

  /** Constructor takes a corpus file parser callback object. */
  public CorpusExtractor(CorpusFileParser corpusFileParser) {
    this.corpusFileParser = corpusFileParser;
  }

  private void parseFiles(Vector<File> files) {
    System.out.println(
        "Found a total of " + files.size() + " corpus files.");
    int totalFiles = files.size();
    int parsedFiles = 0;
    for (File file : files) {
      try {
        parsedFiles++;
        System.out.println(
            "Now extracting corpus file (" + parsedFiles +
            "/" + totalFiles + "): " + file.getName());
        corpusFileParser.parseFile(file);
      } catch (IOException e) {
        e.printStackTrace();
        System.err.println("Error! Could not parse file '" +
                           file.getName() + "'.");
      } catch (Exception e) {
        e.printStackTrace();
        System.err.println("File format exception on file '" +
                           file.getName() + "'.");
      }
    }
    System.out.println("Finished parsing.");
  }

  private void browseForFiles(
      File root,
      Vector<File> files,
      String extension) {
    if (root.isFile()) {
      if (extension == null || root.getName().endsWith(extension)) {
        files.add(root);
      }
    } else if (root.isDirectory()) {
      for (File entry : root.listFiles()) {
        browseForFiles(entry, files, extension);
      }
    }
  }

  /** Parses a corpus using the callback function.
   * 
   * @param fileOrDirName Either a file or a directory to be
   * searched for files.
   * @param extension If non-null, only files matching this extension
   * will be parsed.
   */
  public void parseCorpus(String fileOrDirName, String extension) {
    File root = new File(fileOrDirName);
    Vector<File> files = new Vector<File>();
    browseForFiles(root, files, extension);
    parseFiles(files);
  }
}
