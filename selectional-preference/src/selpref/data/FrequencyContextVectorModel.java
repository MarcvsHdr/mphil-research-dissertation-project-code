package selpref.data;

import java.io.File;

public class FrequencyContextVectorModel extends VectorModel {

  private static final long serialVersionUID = -7921857646660365584L;

  protected Double getVectorValue(String key, String dimension)
      throws Exception {
        return occurrenceModel.getKeyArgumentFrequency(key, dimension);
      }

  public FrequencyContextVectorModel(
      OccurrenceModel occurrenceModel) throws Exception {
    super();
    this.occurrenceModel = occurrenceModel;
  }

  public FrequencyContextVectorModel(File inputFile) throws Exception {
    super(inputFile);
  }
}
