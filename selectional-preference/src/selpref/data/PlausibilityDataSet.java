package selpref.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.*;

import utils.VectorUtils;

public class PlausibilityDataSet {

  private static String GOLDEN_STANDARD_COLUMN = "Golden Standard";
  private static Double UNKNOWN_PLAUSIBILITY = 0.0D;

  class Tuple {
    String verb;
    String argument;
    String function;
    String comments;

    public Tuple(
        String verb,
        String argument,
        String function) {
      this.verb = verb;
      this.argument = argument;
      this.function = function;
      this.comments = new String();
    }
  }

  // A plausibility data set contains a list of tuples to test,
  // and lists of measurements.
  Vector<Tuple> tuples;
  TreeMap<String, Vector<Double>> plausibilities;

  // A plausibility data set also has a number of estimators.
  Vector<PlausibilityEstimator> estimators;

  public PlausibilityDataSet() {
    this.tuples = new Vector<Tuple>();
    this.plausibilities = new TreeMap<String, Vector<Double>>();
    this.estimators = new Vector<PlausibilityEstimator>();
  }

  /** Adds a column of estimates based on a plasubility estimator. */
  public void addEstimates(PlausibilityEstimator estimator)
      throws Exception {
        // Cannot add the same estimator twice.
        if (this.plausibilities.containsKey(estimator.getName())) {
          throw new Exception (
              "Error! Cannot have two estimators with " +
              "identical names ('" + estimator.getName() + "').");
        }

        // Create and populate new vector of estimates.
        Vector<Double> estimates = new Vector<Double>();
        for (Tuple tuple : this.tuples) {
          try {
            estimates.add(estimator.assignPlausibily(
                    tuple.verb, tuple.argument));
          } catch (Exception e) {
            estimates.add(UNKNOWN_PLAUSIBILITY);
            tuple.comments = tuple.comments.concat(
                "[" + estimator.getName() + ": " + e.getMessage() + "]");
          } finally {
            System.err.println("Finished estimating (" + estimates.size() +
                               "/" + this.tuples.size() + ").");
          }
        }
        // Add the estimator and the estimates.
        this.estimators.add(estimator);
        this.plausibilities.put(estimator.getName(), estimates);
      }

  /** Loads a .golden_standard from a buffered reader. */
  private void parseFromBufferedReader(BufferedReader in) throws Exception {
    String line;
    Vector<Double> gsplausibilities = new Vector<Double>();
    do {
      line = in.readLine();
      if (line != null) {
        String[] tokens = line.split(" ");
        if (tokens.length != 4) {
          throw new Exception("Unrecognized syntax in tuple line '" +
                              line + "'. Skipping...");
        } else {
          tuples.add(new Tuple(tokens[0], tokens[1], tokens[2]));
          gsplausibilities.add(Double.parseDouble(tokens[3]));
        }
      }
    } while (line != null);
    System.err.println("\tDone!");
    this.plausibilities.put(GOLDEN_STANDARD_COLUMN, gsplausibilities);
  }

  /** Loads a .golden_standard from file. */
  public void parseFromFile(File inputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!inputFile.getName().endsWith(".golden_standard")) {
      throw new Exception(
          "Error when trying to load GoldenStandard from file '" +
          inputFile.getName() +
          "'. Expected extension '.golden_standard'"
          );
    } else {
      System.err.println("Loading GoldenStandard from file '"
                         + inputFile.getName() + "'...");
    }

    // Try creating a BufferedReader from the file.
    BufferedReader in = new BufferedReader(new FileReader(inputFile));
    parseFromBufferedReader(in);			
    in.close();

    System.err.println("\tDone!");
  }

  /** Writes a plausibility data set to a file. */
  public void writeToCSVFile(File outputFile) throws Exception {
    // Check that the extension of the file is .topic_model
    if (!outputFile.getName().endsWith(".csv")) {
      throw new Exception(
          "Error when trying to write results to file '" +
          outputFile.getName() +
          "'. Expected extension '.csv'"
          );
    } else {
      System.err.println("Writing results to file '"
                         + outputFile.getName() + "'...");
    }

    // Try creating a PrintWriter from the file.
    PrintStream out = new PrintStream(outputFile);
    writeToCSVPrintStream(out);			
    out.close();

    System.err.println("\tDone!");
  }

  /** Writes a plausibility data set to print stream. */
  public void writeToCSVPrintStream(PrintStream out) throws Exception {
    StringBuffer line;

    // Write the header.
    line = new StringBuffer();
    line.append("Verb, Argument, Relation, " + GOLDEN_STANDARD_COLUMN);
    for (PlausibilityEstimator estimator : this.estimators) {
      line.append(", \"" + estimator.getName() + "\"");
    }
    line.append(", Comments");
    out.println(line);

    // Write the contents.
    for (int tuple_index = 0;
         tuple_index < this.tuples.size();
         ++tuple_index) {
      line = new StringBuffer();
      Tuple tuple = this.tuples.elementAt(tuple_index);
      line.append(tuple.verb + ", " + tuple.argument + ", " + tuple.function);
      line.append(", " + this.plausibilities.get(
              GOLDEN_STANDARD_COLUMN).elementAt(tuple_index));
      for (PlausibilityEstimator estimator : this.estimators) {
        line.append(", " + this.plausibilities.get(
                estimator.getName()).elementAt(tuple_index));
      }
      line.append(", \"" + tuple.comments + "\"");
      out.println(line);
    }

    // Write the Pearson R summaries.
    line = new StringBuffer();
    line.append("\"Pearson R\", , , N/A");
    for (PlausibilityEstimator estimator : this.estimators) {
      line.append(", " + VectorUtils.computePearsonR(
              this.plausibilities.get(GOLDEN_STANDARD_COLUMN),
              this.plausibilities.get(estimator.getName())));
    }
    out.println(line);

    // Write the Spearman Rho summaries.
    line = new StringBuffer();
    line.append("\"Spearman Rho\", , , N/A");
    for (PlausibilityEstimator estimator : this.estimators) {
      line.append(", " + VectorUtils.computeSpearmanRho(
              this.plausibilities.get(GOLDEN_STANDARD_COLUMN),
              this.plausibilities.get(estimator.getName())));
    }
    out.println(line);
  }
}
