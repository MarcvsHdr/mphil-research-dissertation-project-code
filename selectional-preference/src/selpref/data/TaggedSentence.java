package selpref.data;

import java.util.TreeMap;
import java.util.TreeSet;

public class TaggedSentence {

  public final static String lexemRegex = "^[\\p{L}-]*$";

  protected class Word {
    String pos;
    String extended_pos;
    String lemma;

    public Word(String pos, String extended_pos, String lemma) {
      this.pos = pos;
      this.extended_pos = extended_pos;
      this.lemma = lemma;
    }
  }

  // Sentence words.
  protected TreeMap<Integer, Word> words;

  protected void clear() {
    words = new TreeMap<Integer, Word>();
  }

  // The constructor of a sentence.
  public TaggedSentence() {
    this.clear();
  }

  /** Adds a word to the sentence. */
  public void addWord(
      Integer sentenceIndex,
      String pos,
      String extended_pos,
      String lemma) {
    words.put(sentenceIndex, new Word(pos, extended_pos, lemma));
  }

  /** Counts the contexts of the words in the sentence (possibly filtering by
   * POS) to an occurrence model.
   *
   * @param occurrenceModel
   * @param headPos Leave null for any head POS.
   * @param dependentPos Leave null for any dependent POS.
   * @param wordWindow How many words to the left and right to look.
   */
  public void countWordContextsToOccurrenceModel(
      OccurrenceModel occurrenceModel,
      TreeSet<String> vocabulary,
      String headPos,
      String dependentPos,
      Integer wordWindow) {
    for (Integer wordIndex : words.keySet()) {
      Word word = words.get(wordIndex);

      // If we're filtering by head POS.
      if (headPos != null && !word.pos.equalsIgnoreCase(headPos)) {
        continue;
      }

      // We are always filtering non-words out.
      if (!word.lemma.matches(lexemRegex)) {
        continue;
      }

      // If we're filtering by vocabulary.
      if (vocabulary != null && !vocabulary.contains(word.lemma)) {
        continue;
      }

      for (Integer contextWordIndex = wordIndex - wordWindow;
           contextWordIndex <= wordIndex + wordWindow;
           contextWordIndex++) {
        if (/*contextWordIndex != wordIndex &&*/
            words.containsKey(contextWordIndex)) {
          Word contextWord = words.get(contextWordIndex);

          // If we're filtering by context POS.
          if (dependentPos != null &&
              !contextWord.pos.equalsIgnoreCase(dependentPos)) {
            continue;
          }

          // We're always filtering non-words out.
          if (!contextWord.lemma.matches(lexemRegex)) {
            continue;
          }

          // If we're filtering by vocabulary.
          if (vocabulary != null &&
              !vocabulary.contains(contextWord.lemma)) {
            continue;
          }

          // Count.
          String keyToAdd = word.lemma +
              "--" + word.pos.charAt(0);
          String argumentToAdd = contextWord.lemma +
              "--" + contextWord.pos.charAt(0);
          occurrenceModel.addTriple(
              keyToAdd, "context", argumentToAdd);
        }
      }
    }
  }
}
