package selpref.data;

public interface PlausibilityEstimator {

  /** The name of the estimator. */
  public String getName();

  /** The plausibility assigned by the estimator. */
  public Double assignPlausibily(String verb, String argument)
      throws Exception;

}
