package selpref.data;

import java.util.*;
import java.util.Map.Entry;
import java.io.*;

/** An occurrence model is a tabular data structure that maps a type of
 * keys (e.g. verb lemmas) and a relation tag (e.g. the "dobj" grammatical
 * relation) to a set of arguments (e.g. verb arguments). This data structure
 * can be used for the following types of queries:
 * 
 * (a) What is the set of arguments for a given key?
 * 
 * (b) What is the relative frequency for the argument of a given key?
 *
 * (c) What is the overall frequency of a given argument?
 * 
 * (d) What is the PMI of a (key, argument) pair?
 * 
 * The data structure is serialized/de-serialized from plain-text files
 * called .occurrence files, which have the following syntax:
 * (Key <tab> Relation <tab> (Argument <space>)* Argument)+
 * 
 * The .occurrence file syntax allows whole-line comments (lines beginning
 * with '#'). Arguments have to be alphanumeric, strings without
 * spaces or tabs.
 * 
 * 
 * @author Adrian Scoică (adriansc@rosedu.org)
 *
 */
public class OccurrenceModel implements Serializable {

  private static final long serialVersionUID = 4215598312367338395L;

  private final String UNDEFINED_RELATION = new String("undefined");

  private TreeMap<String, TreeMap<String, Integer>> keyArgumentFrequencyMap;
  private String secondary_key;
  private TreeMap<String, Integer> keyFrequencyMap;
  private TreeMap<String, Integer> argumentFrequencyMap;
  private Integer totalInstances;

  /** Builds the inverted occurrence model. */
  public OccurrenceModel getInvertedModel() {
    OccurrenceModel returnValue = new OccurrenceModel();
    System.err.println("Inverting occurrence model...");
    Integer inverted = 0;
    for (String key : this.keyArgumentFrequencyMap.keySet()) {
      TreeMap<String, Integer> argumentMap =
          this.keyArgumentFrequencyMap.get(key);
      for (String argument : argumentMap.keySet()) {
        Integer count = argumentMap.get(argument);
        for (int i = 0; i < count; ++i) {
          returnValue.addTriple(
              argument, this.secondary_key, key, count);
        }
      }
      inverted = inverted + 1;
      if (inverted % 1000 == 0) {
        System.err.println("Inverted (" + inverted + "/" +
                           this.keyArgumentFrequencyMap.keySet().size() + ")");
      }
    }
    System.err.println("\tDone!");
    return returnValue;
  }

  /** Purges deletes arguments that are not in the set gives as parameter. */
  public void purgeArguments(Set<String> allowedArguments) {
    System.err.println("Purging OccurrenceModel (reducing to " +
                       allowedArguments.size() + " arguments)...");
    Integer newTotalInstances = 0;

    // Iterate over keys and delete forbidden arguments.
    Iterator<Entry<String, TreeMap<String, Integer>>> keyArgumentFrequency =
        this.keyArgumentFrequencyMap.entrySet().iterator();
    while (keyArgumentFrequency.hasNext()) {
      Map.Entry<String, TreeMap<String, Integer>> keyArgumentFreqEntry =
          keyArgumentFrequency.next();
      String key = keyArgumentFreqEntry.getKey();
      TreeMap<String, Integer> argumentFrequencyMapForKey =
          keyArgumentFreqEntry.getValue();
      Integer newKeyFrequency = 0;

      // Iterate over the key's arguments to delete the forbidden ones.
      Iterator<Map.Entry<String, Integer>> argumentFrequencyForKey =
          argumentFrequencyMapForKey.entrySet().iterator();
      while (argumentFrequencyForKey.hasNext()) {
        // Get the next iterator.
        Map.Entry<String, Integer> entry =
            argumentFrequencyForKey.next();
        // If the entry is not allowed, delete it. Otherwise, count it.
        if (!allowedArguments.contains(entry.getKey())) {
          argumentFrequencyForKey.remove();
        } else {
          newKeyFrequency += entry.getValue();
        }
      }

      // If the new key frequency reached 0, remove the key altogether.
      if (newKeyFrequency == 0) {
        keyArgumentFrequency.remove();
        this.keyFrequencyMap.remove(key);
      } else {
        this.keyFrequencyMap.put(key, newKeyFrequency);
      }

      // Count in the new key frequency to the total number of instances.
      newTotalInstances += newKeyFrequency;
    }

    // Iterate over the arguments and delete the ones that are not allowed.
    Iterator<Map.Entry<String, Integer>> argumentFrequency =
        this.argumentFrequencyMap.entrySet().iterator();
    while (argumentFrequency.hasNext()) {
      Map.Entry<String, Integer> entry = argumentFrequency.next();
      if (!allowedArguments.contains(entry.getKey())) {
        argumentFrequency.remove();
      }
    }

    // Put back the new total instance count.
    System.err.println("\tDone! (compressed to " +
                       ((((double)newTotalInstances) / this.totalInstances) *
                        100) +
                       "%)"); 
    this.totalInstances = newTotalInstances;
  }

  /** Adds "count" instances (key, relation, argument) to the structure. */
  public void addTriple(
      String key, String relation, String argument, Integer count) {
    // Add (key, argument) to the keyArgumentFrequencyMap.
    if (!this.keyArgumentFrequencyMap.containsKey(key)) {
      this.keyArgumentFrequencyMap.put(key, new TreeMap<String, Integer>());
    }
    if (!this.keyArgumentFrequencyMap.get(key).containsKey(argument)) {
      this.keyArgumentFrequencyMap.get(key).put(argument,
                                                new Integer(1));
    } else {
      this.keyArgumentFrequencyMap.get(key).put(
          argument,
          this.keyArgumentFrequencyMap.get(key).get(argument) + count);
    }

    // Check that relation matches.
    if (this.secondary_key == null) {
      this.secondary_key = relation;
    } else if (!this.secondary_key.equals(relation)) {
      System.err.println(
          "Warning! Multiple relations in same OccurrenceModel " +
          "for key '" + key + "': '" + this.secondary_key +
          "' and '" + relation + "'");
      this.secondary_key = UNDEFINED_RELATION;
    }

    // Add (key) to the keyFrequencyMap.
    if (!this.keyFrequencyMap.containsKey(key)) {
      this.keyFrequencyMap.put(key, new Integer(count));
    } else {
      this.keyFrequencyMap.put(key, this.keyFrequencyMap.get(key) + count);
    }

    // Add (argument) to the argumentFrequencyMap.
    if (!this.argumentFrequencyMap.containsKey(argument)) {
      this.argumentFrequencyMap.put(argument, new Integer(count));
    } else {
      this.argumentFrequencyMap.put(
          argument,
          this.argumentFrequencyMap.get(argument) + count);
    }

    // Count one more relation to the total count of instances.
    this.totalInstances = this.totalInstances + count;
  }

  /** Adds an instance (key, relation, argument) to the data structure. */
  public void addTriple(String key, String relation, String argument) {
    this.addTriple(key, relation, argument, 1);
  }

  /** Clears all the data from an occurrence model. */
  private void clear() {
    this.keyArgumentFrequencyMap =
        new TreeMap<String, TreeMap<String, Integer>>();
    this.keyFrequencyMap = new TreeMap<String, Integer>();
    this.argumentFrequencyMap = new TreeMap<String, Integer>();	
    this.totalInstances = new Integer(0);
  }

  /** Builds an empty occurrence model. */
  public OccurrenceModel() {
    this.clear();
  }

  /** Loads an occurrence model from file. */
  public OccurrenceModel(File inputFile) throws Exception {
    this();
    this.parseFromFile(inputFile);
  }

  /** Loads an occurrence model from a buffered reader. */
  public void parseFromBufferedReader(BufferedReader in) throws Exception {
    // Parse in each line.
    String line;
    Integer parsed = 0;
    while ((line = in.readLine()) != null) {
      // Skip comment lines.
      if (line.startsWith("#")) {
        continue;
      }

      // Split the line into tokens and parse it in.
      String[] tokens = line.split("[\\t]+");
      if (tokens.length != 3) {
        throw new Exception(
            "Error when trying to parse line '" + line + "'.");
      } else {
        String key = tokens[0];
        String relation = tokens[1];
        tokens = tokens[2].split("[ ]+");
        for (int i = 0; i < tokens.length; ++i) {
          this.addTriple(key, relation, tokens[i]);
        }
        parsed = parsed + 1;
        if (parsed % 1000 == 0) {
          System.err.println("Loaded occurrences for (" +
                             parsed + "/?) keys...");
        }
      }
    }
  }

  /** Loads an occurrence model from an .occurrence file. */
  public void parseFromFile(File inputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!inputFile.getName().endsWith(".occurrences")) {
      throw new Exception(
          "Error when trying to load OccurrenceModel from file '" +
          inputFile.getName() +
          "'. Expected extension '.occurrences'"
          );
    } else {
      System.err.println("Loading OccurrenceModel from file '"
                         + inputFile.getName() + "'...");
    }

    // Try creating a BufferedReader from the file.
    BufferedReader in = new BufferedReader(new FileReader(inputFile));
    parseFromBufferedReader(in);			
    in.close();

    System.err.println("\tDone!");
  }

  /** Dumps an occurrence model to a print stream. */
  public void writeToPrintStream(PrintStream out) {
    // Print out the contents.
    for (String key : this.keyArgumentFrequencyMap.keySet()) {
      StringBuffer line = new StringBuffer();
      line.append(key + "\t" + this.secondary_key + "\t");
      TreeMap<String, Integer> argumentMapForKey =
          this.keyArgumentFrequencyMap.get(key);
      boolean firstPrintedArgumentInstance = true;
      for (String argument : argumentMapForKey.keySet()) {
        Integer argumentCount = argumentMapForKey.get(argument);
        for (int i = 0; i < argumentCount; ++i) {
          line.append((firstPrintedArgumentInstance ? "" : " ") +
                      argument);
          firstPrintedArgumentInstance = false;
        }
      }
      out.println(line);
    }		
  }

  /** Saves an occurrence model to an .occurrence file. */
  public void writeToFile(File outputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!outputFile.getName().endsWith(".occurrences")) {
      throw new Exception(
          "Error when trying to write OccurrenceModel to file '" +
          outputFile.getName() +
          "'. Expected extension '.occurrences'"
          );
    } else {
      System.err.println("Writing OccurrenceModel to file'" +
                         outputFile.getName() + "'...");
    }

    // Open a print stream from the file and write the contents there.
    PrintStream out = new PrintStream(outputFile);
    writeToPrintStream(out);
    out.close();

    System.err.println("\tDone!");
  }

  /** Returns all of the keys in this occurrence model. */
  public Set<String> getKeys() {
    return this.keyFrequencyMap.keySet();
  }

  /** Returns all of the arguments for a given key in this model. */
  public Set<String> getArguments(String key) throws Exception {
    if (!this.keyArgumentFrequencyMap.containsKey(key)) {
      throw new Exception("Error. Key '" + key +
                          "' does not exist in occurrence model.");
    }
    return this.keyArgumentFrequencyMap.get(key).keySet();
  }

  /** Returns the top-most frequent N keys. */
  public Vector<String> getMostFrequentKeys(Integer n, String posSuffix)
      throws Exception {
    // Compile and sort descendingly a list of frequency pairs.
    class IntegerKeyPair implements Comparable<IntegerKeyPair> {
      Integer integer;
      String key;

      public IntegerKeyPair(Integer integer, String key) {
        this.integer = integer;
        this.key = key;
      }

      public int compareTo(IntegerKeyPair o) {
        return - (this.integer - o.integer);
      }
    }

    Vector<IntegerKeyPair> pairs = new Vector<IntegerKeyPair>();
    for (String key : this.keyFrequencyMap.keySet()) {
      if (posSuffix == null || (posSuffix != null && key.endsWith(posSuffix))) {
        System.err.println("Adding key == " + key);
        pairs.add(new IntegerKeyPair(this.keyFrequencyMap.get(key), key));
      }
    }
    Collections.sort(pairs);

    // Throw an exception if there are not enough distinct keys in model.
    if (pairs.size() < n) {
      throw new Exception ("Error. Cannot return " + n + " distinct " +
                           "keys. Only " + pairs.size() +
                           " keys in model.");
    }

    // Pick only the first n keys and return them.
    Vector<String> returnValue = new Vector<String>();
    for (int i = 0; i < n; ++i) {
      System.err.println("Frequency order #" + i + ": (" +
                         pairs.elementAt(i).key + ", " +
                         pairs.elementAt(i).integer + ")");
      returnValue.add(pairs.elementAt(i).key);
    }
    return returnValue;
  }

  /** Returns the top-most frequent N arguments. */
  public Vector<String> getMostFrequentArguments(Integer n) throws Exception {
    // Throw an exception if there are not enough distinct arguments in model.
    if (this.argumentFrequencyMap.size() < n) {
      throw new Exception ("Error. Cannot return " + n + " distinct " +
                           "arguments. Only " +
                           this.argumentFrequencyMap.size() +
                           " arguments in model.");
    }

    // Compile and sort descendingly a list of frequency pairs.
    class IntegerKeyPair implements Comparable<IntegerKeyPair> {
      Integer integer;
      String key;

      public IntegerKeyPair(Integer integer, String key) {
        this.integer = integer;
        this.key = key;
      }

      public int compareTo(IntegerKeyPair o) {
        return - (this.integer - o.integer);
      }
    }

    Vector<IntegerKeyPair> pairs = new Vector<IntegerKeyPair>();
    for (String key : this.argumentFrequencyMap.keySet()) {
      pairs.add(new IntegerKeyPair(this.argumentFrequencyMap.get(key), key));
    }
    Collections.sort(pairs);

    // Pick only the first n keys and return them.
    Vector<String> returnValue = new Vector<String>();
    for (int i = 0; i < n; ++i) {
      System.err.println("Frequency order #" + i + ": (" +
                         pairs.elementAt(i).key + ", " +
                         pairs.elementAt(i).integer + ")");
      returnValue.add(pairs.elementAt(i).key);
    }
    return returnValue;
  }

  /** Gives the relative frequency of an argument for a given key, which is
   *  the non-smoothed probability P(argument|key).
   *  Throws an exception if the key does not exist. */
  public Double getRelativeKeyArgumentFrequency(String key, String argument)
      throws Exception {
        if (!this.keyArgumentFrequencyMap.containsKey(key)) {
          throw new Exception(
              "Key '" + key + "' does not exist in the OccurrenceModel");
        }

        if (!this.keyArgumentFrequencyMap.get(key).containsKey(argument)) {
          return new Double(0.0);
        }

        return new Double(this.keyArgumentFrequencyMap.get(key).get(argument)) /
            new Double(this.keyFrequencyMap.get(key));
      }

  /** Gives the relative joint frequency of an argument for a given key,
   *  which is the non-smoothed probability P(argument, key).
   *  Throws an exception if the key does not exist. */
  public Double getRelativeJointKeyArgumentFrequency(String key, String argument)
      throws Exception {
        if (!this.keyArgumentFrequencyMap.containsKey(key)) {
          throw new Exception(
              "Key '" + key + "' does not exist in the OccurrenceModel");
        }

        if (!this.keyArgumentFrequencyMap.get(key).containsKey(argument)) {
          return new Double(0.0);
        }

        return new Double(this.keyArgumentFrequencyMap.get(key).get(argument)) /
            new Double(this.totalInstances);
      }

  /** Gives the relative frequency of a given key in the occurrence model,
   * which is the non-smoothed probability P(key).
   * Throws an exception if the key does not exist. */
  public Double getRelativeKeyFrequency(String key) throws Exception {
    if (!this.keyFrequencyMap.containsKey(key)) {
      throw new Exception(
          "Key '" + key + "' does not exist in the OccurrenceModel");
    }

    return new Double(this.keyFrequencyMap.get(key)) /
        new Double(this.totalInstances);
  }

  /** Gives the relative frequency of a given argument in the occurrence model,
   * which is the non-smoothed probability P(argument). */
  public Double getRelativeArgumentFrequency(String argument) {
    if (!this.argumentFrequencyMap.containsKey(argument)) {
      return 0.0D;
    }

    return new Double(this.argumentFrequencyMap.get(argument)) /
        new Double(this.totalInstances);
  }

  /** Gives the count for a (key, argument) pair in the occurrence model. */
  public Double getKeyArgumentFrequency(String key, String argument)
      throws Exception {
        if (!this.keyArgumentFrequencyMap.containsKey(key)) {
          throw new Exception(
              "Key '" + key + "' does not exist in the OccurrenceModel");
        }

        if (!this.keyArgumentFrequencyMap.get(key).containsKey(argument)) {
          return new Double(0.0);
        }

        return new Double(this.keyArgumentFrequencyMap.get(key).get(argument));
      }

  /** Gives the PMI (mutual information) of a (key, argument) pair. */
  public Double getPMI(String key, String argument) throws Exception {
    // Compute piecewise probabilities.
    Double joint_probability =
        getRelativeJointKeyArgumentFrequency(key, argument);
    Double key_probability = getRelativeKeyFrequency(key);
    Double argument_probability = getRelativeArgumentFrequency(argument);

    // Compose the PMI score.
    if (argument_probability == 0.0D || key_probability == 0.0D) {
      return 0.0D;
    } else {
      return joint_probability /
          (key_probability * argument_probability);
    }
  }

  /** Gives log(PMI) of a (key, argument) pair. */
  public Double getLogPMI(String key, String argument) throws Exception {
    // Compute piecewise probabilities.
    Double joint_probability =
        getRelativeJointKeyArgumentFrequency(key, argument);
    Double key_probability = getRelativeKeyFrequency(key);
    Double argument_probability = getRelativeArgumentFrequency(argument);

    // Compose the PMI score.
    if (argument_probability == 0.0D ||
        key_probability == 0.0D ||
        joint_probability == 0.0D) {
      return 0.0D;
    } else {
      return Math.log(joint_probability) -
          Math.log(key_probability) -
          Math.log(argument_probability);
    }
  }

  /** Gives the count of a (key, argument) pair. */
  public Integer getCount(String key, String argument) throws Exception {
    if (!this.keyArgumentFrequencyMap.containsKey(key)) {
      throw new Exception(
          "Key '" + key + "' does not exist in the OccurrenceModel");
    }

    if (!this.keyArgumentFrequencyMap.get(key).containsKey(argument)) {
      return 0;
    }

    return this.keyArgumentFrequencyMap.get(key).get(argument);
  }

  /** Return the secondary key of the occurrence mode. */
  public String getSecondaryKey() {
    return this.secondary_key;
  }

  /** Get the number of instances in the model. */
  public Integer size() {
    return this.totalInstances;
  }
}
