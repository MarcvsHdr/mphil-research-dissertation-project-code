package selpref.data;

import java.util.*;
import java.io.*;

public class PMISyntacticVectorModel extends VectorModel {

  private static final long serialVersionUID = -6024639533453112628L;

  protected Double getVectorValue(String key, String dimension)
      throws Exception {
        return occurrenceModel.getPMI(key, dimension);
      }

  public PMISyntacticVectorModel(
      OccurrenceModel occurrenceModel,
      Integer numberDimensions) throws Exception {
    this.occurrenceModel = occurrenceModel;
    this.dimensions =
        occurrenceModel.getMostFrequentArguments(numberDimensions);
    this.vectors = new TreeMap<String, Vector<Double>>();
    this.occurrenceModel.purgeArguments(
        new TreeSet<String>(this.dimensions));
  }

  public PMISyntacticVectorModel(File inputFile) throws Exception {
    super(inputFile);
  }
}
