package selpref.data;

import java.io.File;

/** Callback for the corpus extractor. */
public abstract class CorpusFileParser {
  public void parseFile(File file) throws Exception {
    throw new Exception("Not implemented." +
                        "Please implement this method for the " +
                        "corpus you are parsing.");
  }
}
