package selpref.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

/** A topic model is a data structure that encodes verb and argument topics,
 * and that can answer the following types of questions:
 * 
 * (a) What is the joint probability P(verb, argument) according to the
 * classical LDA model?
 * 
 *  (b) What is the joint probability P(verb, argument) according to the
 * RoothLDA model?
 * 
 * The data structure is de-serialized from plain-text files
 * called .topic_model files, which have the following syntax:
 * (AlphaParameter <space>)+ AlphaParameter
 * BetaParameter
 * (Verb <space>)* Verb
 * (Topic <space> Occurrences <tab>)* (Topic <space> Occurrences){#Verbs}
 * (Argument <space>) * Argument
 * (Topic <space> Occurrences <tab>)* (Topic <space> Occurrences){#Arguments}
 * 
 * The .topic_model file syntax allows whole-line comments (lines beginning
 * with '#'). Arguments have to be alphanumeric, strings without
 * spaces or tabs.
 * 
 * 
 * @author Adrian Scoică (adriansc@rosedu.org)
 *
 */
public class TopicModel {

  private class Distribution {
    private Integer totalOccurrences = 0;
    private TreeMap<Integer, Integer> counts =
        new TreeMap<Integer, Integer>();

    public void add(Integer key, Integer occurrences) {
      if (counts.containsKey(key)) {
        counts.put(key, counts.get(key) + occurrences);
      } else {
        counts.put(key, occurrences);
      }
      totalOccurrences += occurrences;
    }

    public int getOccurrences() {
      return totalOccurrences;
    }

    public int getOccurrences(Integer key) {
      if (counts.containsKey(key)) {
        return counts.get(key);
      } else {
        return 0;
      }
    }

    public double getSmoothedProbability(
        Integer key,
        double numerator,
        double denominator) {
      if (counts.containsKey(key)) {
        return ((double)counts.get(key) + numerator) /
            (totalOccurrences + denominator);
      } else {
        return ((double) numerator) / (totalOccurrences + denominator);
      }
    }
  }

  // A topic model incurs a number of topics,
  // data dictionaries and distributions.
  private Set<Integer> topicIDs;
  private TreeMap<String, Integer> verbDictionary;
  private TreeMap<String, Integer> argumentDictionary;
  private TreeMap<Integer, Distribution> verbDistributions;
  private Distribution allVerbsTopicDistribution;
  private TreeMap<Integer, Distribution> argumentDistributions;
  private Distribution allArgumentsTopicDistriubtion;
  private Vector<Double> alphas;
  private Double sum_alphas;
  private Double beta;
  private Double gamma;

  private TopicModel() {
    this.topicIDs = new TreeSet<Integer>();
    this.verbDictionary = new TreeMap<String, Integer>();
    this.argumentDictionary = new TreeMap<String, Integer>();
    this.verbDistributions = new TreeMap<Integer, Distribution>();
    this.allVerbsTopicDistribution = new Distribution();
    this.argumentDistributions = new TreeMap<Integer, Distribution>();
    this.allArgumentsTopicDistriubtion = new Distribution();
    this.alphas = new Vector<Double>();
    this.gamma = 0.01D; /* TODO -- sort out this magic number */
  }

  /** Returns the verbs known in the topic model. **/
  public TreeSet<String> getKnownVerbs() {
    return new TreeSet<String>(this.verbDictionary.keySet());
  }

  /** Returns arguments known in the topic model. **/
  public TreeSet<String> getKnownArguments() {
    return new TreeSet<String>(this.argumentDictionary.keySet());
  }

  /** Constructor that loads a topic model from file. */
  public TopicModel(File inputFile) throws Exception {
    this();
    this.parseFromFile(inputFile);
  }

  /** Computes P(argument,verb) = P(verb) *
   *          [sum_{t \in topics} P(argument|topic) * P(topic|verb)] */
  public double getJointProbabilityArgumentVerbClassicalLDA(
      String verb,
      String argument) throws Exception {
    double returnValue = 0.0f;

    // Get the IDs.
    Integer verbID = verbDictionary.get(verb);
    if (verbID == null) {
      System.err.println("Unknown verb '" + verb + "'.");
      throw new Exception("Unknown verb '" + verb + "'.");
    }
    Integer argumentID = argumentDictionary.get(argument);
    if (argumentID == null) {
      System.err.println("Unknown argument '" + argument + "'.");
      throw new Exception("Unknown argument '" + argument + "'.");
    }

    // Iterate over the distributions.
    for (Integer topicID : topicIDs) {
      // Compute P(argument|topic)
      double p_argument_given_topic =
          ((double)argumentDistributions.get(
                  argumentID).getOccurrences(topicID) + beta) /
          (allArgumentsTopicDistriubtion.getOccurrences(
                  topicID) + argumentDictionary.size() * beta);
      // Compute P(topic|verb)
      double p_topic_given_verb =
          verbDistributions.get(verbID).getSmoothedProbability(
              topicID, alphas.elementAt(topicID), sum_alphas);
      // Add to the total.
      returnValue += p_argument_given_topic * p_topic_given_verb;
    }

    // Multiply with the probability of the verb in the corpus.
    returnValue *=
        (((double)verbDistributions.get(verbID).getOccurrences()) /
         allArgumentsTopicDistriubtion.getOccurrences());

    return returnValue;
  }

  /** Computes P(argument,verb) =
   * 		sum_{t \in topics} P(argument|topic) * P(verb|topic) * P(topic) */
  public double getJointProbabilityArgumentVerbRoothLDA(
      String verb,
      String argument) throws Exception {
    double returnValue = 0.0f;

    // Get the IDs.
    Integer verbID = verbDictionary.get(verb);
    if (verbID == null) {
      System.err.println("Unknown verb '" + verb + "'.");
      throw new Exception("Unknown verb '" + verb + "'.");
    }
    Integer argumentID = argumentDictionary.get(argument);
    if (argumentID == null) {
      System.err.println("Unknown argument '" + argument + "'.");
      throw new Exception("Unknown argument '" + argument + "'.");
    }

    // Iterate over the distributions.
    for (Integer topicID : topicIDs) {
      // Compute P(argument|topic)
      double p_argument_given_topic =
          ((double)argumentDistributions.get(
                  argumentID).getOccurrences(topicID) + beta) /
          (allArgumentsTopicDistriubtion.getOccurrences(topicID) +
           argumentDictionary.size() * beta);
      // Compute P(verb|topic)
      double p_verb_given_topic =
          ((double)verbDistributions.get(
                  verbID).getOccurrences(topicID) + gamma) /
          (allArgumentsTopicDistriubtion.getOccurrences(topicID) +
           verbDictionary.size() * gamma);
      // Compute P(topic)
      double p_topic =
          allArgumentsTopicDistriubtion.getSmoothedProbability(
              topicID, alphas.get(topicID), sum_alphas);
      // Add to the total.
      returnValue +=
          p_argument_given_topic * p_verb_given_topic * p_topic;
    }

    return returnValue;
  }

  /** Loads an occurrence model from a buffered reader. */
  public void parseFromBufferedReader(BufferedReader in) throws Exception {
    // Skip over all comment lines that start with "#".
    String line;
    do {
      line = in.readLine();
      if (line.startsWith("#")) {
        System.err.println("Skipping comment line '" + line + "'.");
      }
    } while (line.startsWith("#"));

    // Parse the alpha parameters for the topics and
    // the beta parameter for arguments.
    String[] line_alphas = line.split(" ");
    sum_alphas = 0.0D;
    for (int i = 0; i < line_alphas.length; ++i) {
      double alpha = Double.parseDouble(line_alphas[i]);
      this.alphas.add(alpha);
      sum_alphas += alpha;
    }
    line = in.readLine();
    this.beta = Double.parseDouble(line);
    line = in.readLine();

    // Parse the verbs.
    String[] verbs = line.split("[ \\t]+");
    System.err.println(
        "Loaded a total of " + verbs.length + " recognized verbs.");
    for (int i = 0; i < verbs.length; ++i) {
      verbDictionary.put(verbs[i], i);
    }

    // For each verb, parse the line with its topic distribution.
    for (int verb = 0; verb < verbDictionary.size(); ++verb) {
      if (verb % 1000 == 0) {
        System.err.println("Loading data for verbs (" + verb + "/" +
                           verbDictionary.size() + ").");
      }
      Distribution d = new Distribution();
      line = in.readLine();
      String[] ints = line.split("[ \\t]+");
      for (int i = (ints[0].length() == 0 ? 1 : 0);
           i + 1 < ints.length;
           i += 2) {
        Integer topicID = Integer.parseInt(ints[i]);
        Integer count = Integer.parseInt(ints[i + 1]);
        d.add(topicID, count);
        allVerbsTopicDistribution.add(topicID, count);
        topicIDs.add(topicID);
      }
      verbDistributions.put(verb, d);
    }
    System.err.println("Done loading data for verbs!");

    // Parse the arguments.
    line = in.readLine();
    String[] arguments = line.split("[\\t ]+");
    for (int i = 0; i < arguments.length; ++i) {
      argumentDictionary.put(arguments[i], i);
    }

    // For each argument, parse the line with its topic distribution.
    for (int argument = 0; argument < argumentDictionary.size(); ++argument) {
      if (argument % 1000 == 0) {
        System.err.println("Loading data for arguments (" + argument +
                           "/" + argumentDictionary.size() + ").");
      }
      Distribution d = new Distribution();
      line = in.readLine();
      String[] ints = line.split("[ \\t]+");
      for (int i = (ints[0].length() == 0 ? 1 : 0);
           i + 1 < ints.length;
           i += 2) {
        Integer topicID = Integer.parseInt(ints[i]);
        Integer count = Integer.parseInt(ints[i + 1]);
        d.add(topicID, count);
        allArgumentsTopicDistriubtion.add(topicID, count);
        topicIDs.add(topicID);
      }
      argumentDistributions.put(argument, d);
    }
    System.err.println("Done loading data for arguments!");
  }

  /** Loads an occurrence model from a .topic_model file. */
  public void parseFromFile(File inputFile) throws Exception {
    // Check that the extension of the file is .topic_model
    if (!inputFile.getName().endsWith(".topic_model")) {
      throw new Exception(
          "Error when trying to load TopicModel from file '" +
          inputFile.getName() +
          "'. Expected extension '.topic_model'"
          );
    } else {
      System.err.println("Loading TopicModel from file '"
                         + inputFile.getName() + "'...");
    }

    // Try creating a BufferedReader from the file.
    BufferedReader in = new BufferedReader(new FileReader(inputFile));
    parseFromBufferedReader(in);			
    in.close();

    System.err.println("\tDone!");
  }
}
