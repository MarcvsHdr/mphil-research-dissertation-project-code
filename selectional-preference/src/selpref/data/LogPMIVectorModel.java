package selpref.data;

import java.io.File;

public class LogPMIVectorModel extends VectorModel {

  private static final long serialVersionUID = 2053189157617031386L;

  protected Double getVectorValue(String key, String dimension)
      throws Exception {
        return occurrenceModel.getLogPMI(key, dimension);
      }

  public LogPMIVectorModel(
      OccurrenceModel occurrenceModel) throws Exception {
    this.occurrenceModel = occurrenceModel;
  }

  public LogPMIVectorModel(File inputFile) throws Exception {
    super(inputFile);
  }
}
