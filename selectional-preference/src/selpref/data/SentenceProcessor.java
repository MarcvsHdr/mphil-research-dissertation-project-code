package selpref.data;

public interface SentenceProcessor {

  public void processSentence(TaggedSentence sentence) throws Exception;

}
