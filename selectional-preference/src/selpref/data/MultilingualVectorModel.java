package selpref.data;

import java.util.*;
import java.io.*;

import utils.VectorUtils;

public class MultilingualVectorModel implements Serializable {

  private static final long serialVersionUID = 6260612713002790999L;

  TreeMap<String, VectorModel> vectorModels;
  MultilingualDimensionModel dimensionModel;

  private void swapData(MultilingualVectorModel other) {
    {
      TreeMap<String, VectorModel> aux = other.vectorModels;
      other.vectorModels = this.vectorModels;
      this.vectorModels = aux;
    }

    {
      MultilingualDimensionModel aux = other.dimensionModel;
      other.dimensionModel = this.dimensionModel;
      this.dimensionModel = aux;
    }
  }

  protected MultilingualVectorModel() {
    this.vectorModels = new TreeMap<String, VectorModel>();
    this.dimensionModel = new MultilingualDimensionModel();
  }

  public MultilingualVectorModel(File inputFile) throws Exception {
    this();
    this.parseFromFile(inputFile);
  }

  public class LanguageKeyPair {
    String language;
    String key;
  }

  private class MultilingualDimensionModel implements Serializable {

    private static final long serialVersionUID = 8191959883203339093L;

    TreeSet<String> languages;
    Vector<TreeMap<String, String>> dimensions;

    public MultilingualDimensionModel() {
      this.languages = new TreeSet<String>();
      this.dimensions = new Vector<TreeMap<String, String>>();
    }

    public MultilingualDimensionModel(
        TreeSet<String> languages,
        String[] seedTranslationFileNames) throws Exception {
      this.languages = languages;
      this.dimensions = new Vector<TreeMap<String, String>>();
      // Parse the seed translation files and extract dimensions.
      for (String inputFile : seedTranslationFileNames) {
        this.parseFromFile(new File(inputFile));
      }
    }

    public void purgeDimensions(TreeMap<String, Set<String>> accepted) {
      Vector<TreeMap<String, String>> purgedDimensions =
          new Vector<TreeMap<String, String>>();
      for (TreeMap<String, String> dimension : this.dimensions) {
        // Check if any of the translations is accepted.
        Boolean dropDimension = true;
        for (String language : this.languages) {
          if (accepted.get(language).contains(
                  dimension.get(language))) {
            dropDimension = false;
            break;
          } else {
          }
        }

        // If none of the translations is accepted, drop it.
        if (dropDimension == false) {
          purgedDimensions.add(dimension);
        }
      }

      // Replace dimensions with the purged version.
      this.dimensions = purgedDimensions;
    }

    /** Loads a translation set from a buffered reader. */
    private void parseFromBufferedReader(BufferedReader in)
        throws Exception {
          String line;

          // The first line contains the languages.
          line = in.readLine();
          String[] languageSet = line.split("[ \\t]+");
          if (this.languages.size() != languageSet.length) {
            throw new Exception ("Failed. Uneven language sets!");
          }
          TreeMap<String, Integer> languagePositions =
              new TreeMap<String, Integer>();
          for (int i = 0; i < languageSet.length; ++i) {
            if (!this.languages.contains(languageSet[i])) {
              throw new Exception("Error. Untracked language '" +
                                  languageSet[i] + "' found.");
            }
            languagePositions.put(languageSet[i], i);
          }

          // Parse in the rest of the translations.
          while ((line = in.readLine()) != null) {
            // Skip comment lines.
            if (line.startsWith("#")) {
              continue;
            }

            String[] translations = line.split("[ \\t]+");
            TreeMap<String, String> multilingualDimension =
                new TreeMap<String, String>();
            Set<String> distinctTranslations = new TreeSet<String>();
            for (String language : this.languages) {
              String translation =
                  translations[languagePositions.get(language)];
              multilingualDimension.put(language, translation);
              distinctTranslations.add(translation);
            }

            // We skip pairs that have the same translation.
            if (distinctTranslations.size() == this.languages.size()) {
              this.addMultilingualDimension(multilingualDimension);
            }
          }
        }

    /** Loads a translation set from a .translations file. */
    private void parseFromFile(File inputFile) throws Exception {
      // Check that the extension of the file is .occurrence
      if (!inputFile.getName().endsWith(".translations")) {
        throw new Exception(
            "Error when trying to load TranslationSet from file '" +
            inputFile.getName() +
            "'. Expected extension '.translations'"
            );
      } else {
        System.err.println("Loading TranslationSet from file '"
                           + inputFile.getName() + "'...");
      }

      // Try creating a BufferedReader from the file.
      BufferedReader in = new BufferedReader(new FileReader(inputFile));
      parseFromBufferedReader(in);
      in.close();

      System.err.println("\tDone!");
    }


    public void addMultilingualDimension(
        TreeMap<String, String> dimension) throws Exception {
      // Check that the same languages are covered.
      if (this.languages.size() != dimension.keySet().size()) {
        throw new Exception(
            "Error. Unequal number of languages tracked.");
      }
      for (String language : this.languages) {
        if (!dimension.keySet().contains(language)) {
          throw new Exception("Error. Language '" + language +
                              "' not covered in new dimension.");
        }
      }

      // Add the new dimension set.
      this.dimensions.add(dimension);
    }

    /** Gets the dimensions for a given language. */
    public Vector<String> getDimensionsForLanguage(String language) {
      System.err.println(
          "Retrieving the dimensions for '" + language + "'...");
      Integer verbDimensions = 0;
      Integer nounDimensions = 0;
      Vector<String> languageDimensions = new Vector<String>();
      for (TreeMap<String, String> multilingualDimension : dimensions) {
        languageDimensions.add(multilingualDimension.get(language));
        if (multilingualDimension.get(language).endsWith("--V")) {
          verbDimensions++;
        } else if (multilingualDimension.get(language).endsWith("--N")) {
          nounDimensions++;
        }
      }
      System.err.println();

      return languageDimensions;
    }

    /** Prints out all dimensions. */
    public void printAllDimensions() {
      Integer dimensionRank = 0;
      Integer verbDimensions = 0;
      Integer nounDimensions = 0;
      for (TreeMap<String, String> dimension : this.dimensions) {
        StringBuffer line = new StringBuffer();
        line.append("Dimension Rank #" + dimensionRank + ": ");
        dimensionRank = dimensionRank + 1;
        for (String language : this.languages) {
          line.append(language + ":" + dimension.get(language) + " ");
        }
        if (dimension.get(this.languages.first()).endsWith("--V")) {
          verbDimensions++;
        } else if (dimension.get(this.languages.first()).endsWith("--N")) {
          nounDimensions++;
        }
        System.err.println(line);
      }
      System.err.println("There are a total of " +
                         (verbDimensions + nounDimensions) + " dimensions:");
      System.err.println("\t" + verbDimensions + " verbs;");
      System.err.println("\t" + nounDimensions + " nouns;");
    }
  }

  public void iterateBilingualDimensions(
      String languageOne,
      String languageTwo,
      String posSuffix,
      Integer maxDimensions,
      Integer truncationFactor) throws Exception {
    if (this.dimensionModel.languages.size() != 2) {
      throw new Exception("We can only iterate dimensions on " +
                          "bilingual vector models.");
    }

    class TranslationPair implements Comparable<TranslationPair> {
      String sourceLexem;
      String targetLexem;
      Double similarity;

      public TranslationPair(String sourceLexem,
                             String targetLexem,
                             Double similarity) {
        this.sourceLexem = sourceLexem;
        this.targetLexem = targetLexem;
        this.similarity = similarity;
      }

      public int compareTo(TranslationPair o) {
        if (this.similarity < o.similarity) {
          return -1;
        } else if (this.similarity > o.similarity) {
          return +1;
        } else {
          return 0;
        }
      }
    }

    PriorityQueue<TranslationPair> pq = new PriorityQueue<TranslationPair>();

    Integer mutualCandidateRank = 1;
    Integer scannedPairs = 0;
    Integer totalLanguageOneKeys = 0;
    for (String key : this.vectorModels.get(
            languageOne).occurrenceModel.getKeys()) {
      if (key.endsWith(posSuffix)) {
        totalLanguageOneKeys++;
      }
    }
    Integer consideredLanguageOneKeys = totalLanguageOneKeys / truncationFactor;
    for (String sourceLexem :
         this.vectorModels.get(
             languageOne).occurrenceModel.getMostFrequentKeys(
                 consideredLanguageOneKeys, posSuffix)) {
      // Get the first mutualCandidateRank neighbors of the source lexem.
      Vector<LanguageKeyVectorSimilarityTuple> candidates =
          this.getClosestKNeighbours(
              languageOne,
              sourceLexem,
              mutualCandidateRank,
              languageTwo,
              null);
      // If the candidates are mutual, add them to the translation pairs.
      for (LanguageKeyVectorSimilarityTuple candidate : candidates) {
        Vector<LanguageKeyVectorSimilarityTuple> mutualCandidates =
            this.getClosestKNeighbours(
                languageTwo,
                candidate.key,
                mutualCandidateRank,
                languageOne,
                null);
        Boolean mutuality = false;
        for (LanguageKeyVectorSimilarityTuple mutualCandidate : mutualCandidates) {
          if (mutualCandidate.key == sourceLexem) {
            mutuality = true;
            break;
          }
        }
        if (mutuality) {
          pq.add(new TranslationPair(sourceLexem,
                                     candidate.key,
                                     candidate.similarity));
          System.err.println("Adding (" + sourceLexem +
                             " - " + candidate.key + ": " +
                             candidate.similarity + ").");
        } else {
          System.err.println("NO MUTUALITY: " + sourceLexem +
                             " --> " + candidate.key + " (--> " +
                             mutualCandidates.get(0).key +")");
        }
      }
      scannedPairs = scannedPairs + 1;
      System.err.println("Scanned pairs (" + scannedPairs + "/" +
                         consideredLanguageOneKeys + ")...");
    }

    // Truncate the priority queue.
    while (pq.size() > maxDimensions) {
      pq.remove();
    }

    // Add the previous dimensions to the priority queue.
    for (TreeMap<String, String> dimension :
         this.dimensionModel.dimensions) {
      String sourceLexem = dimension.get(languageOne);
      String targetLexem = dimension.get(languageTwo);
      pq.add(
          new TranslationPair(
              sourceLexem,
              targetLexem,
              getSimilarity(
                  sourceLexem,
                  languageOne,
                  targetLexem,
                  languageTwo)));
    }

    // Destroy previous dimensions and add a new dimension set from the PQ.
    // Don't allow the same word twice!
    this.dimensionModel.dimensions.clear();
    TreeMap<String, TreeSet<String>> seenLexems =
        new TreeMap<String, TreeSet<String>>();
    for (String language : this.dimensionModel.languages) {
      seenLexems.put(language, new TreeSet<String>());
    }

    while (this.dimensionModel.dimensions.size() < maxDimensions &&
           pq.size() > 0) {
      // Pop a new dimension from the PQ and add it.
      TranslationPair tp = pq.poll();
      TreeMap<String, String> newDimension = new TreeMap<String, String>();
      newDimension.put(languageOne, tp.sourceLexem);
      newDimension.put(languageTwo, tp.targetLexem);
      Boolean uniquePair = true;
      if (seenLexems.get(languageOne).contains(tp.sourceLexem)) {
        System.err.println("Not adding (" + tp.sourceLexem + ", " +
                           tp.targetLexem + ") == " + tp.similarity +
                           " because '" + tp.sourceLexem +
                           "' already exists in the dimension space.");
        uniquePair = false;
      } else {
        seenLexems.get(languageOne).add(tp.sourceLexem);
      }
      if (seenLexems.get(languageTwo).contains(tp.targetLexem)) {
        System.err.println("Not adding (" + tp.sourceLexem + ", " +
                           tp.targetLexem + ") == " + tp.similarity +
                           " because '" + tp.targetLexem +
                           "' already exists in the dimension space.");
        uniquePair = false;
      } else {
        seenLexems.get(languageTwo).add(tp.targetLexem);
      }
      if (uniquePair) {
        this.dimensionModel.addMultilingualDimension(newDimension);
      }
    }

    this.dimensionModel.printAllDimensions();
  }

  public MultilingualVectorModel(
      TreeMap<String, File> inputFiles,
      Integer numberOfTopmostFrequentDimensions,
      String[] translationPairFileNames) 
      throws Exception {
        // Load up the vector models from the input files.
        this.vectorModels = new TreeMap<String, VectorModel>();
        Integer vectorModelToLoad = 0;
        for (String language : inputFiles.keySet()) {
          vectorModelToLoad = vectorModelToLoad + 1;
          this.vectorModels.put(language,
                                new FrequencyContextVectorModel(
                                    inputFiles.get(language)));
        }
        // Initialize the dimension set.
        this.initializeFrequentDimensions(
            numberOfTopmostFrequentDimensions,
            translationPairFileNames);
      }

  private void initializeFrequentDimensions(
      Integer numberOfTopmostFrequentDimensions,
      String[] translationPairFileNames) throws Exception {
    // Create the dimension model with the languages from
    // the current vector models.
    this.dimensionModel = new MultilingualDimensionModel(
        new TreeSet<String>(this.vectorModels.keySet()),
        translationPairFileNames);

    // We're going to throw away any keys that are not found in any of
    // the languages.
    Vector<TreeMap<String, String>> validDimensions =
        new Vector<TreeMap<String, String>>();
    for (TreeMap<String, String> multilingualDimension :
         this.dimensionModel.dimensions) {
      Boolean validDimension = true;
      for (String language : this.vectorModels.keySet()) {
        String languageDimension = multilingualDimension.get(language);
        VectorModel languageVectorModel =
            this.vectorModels.get(language);
        if (!languageVectorModel.getKeys().contains(languageDimension)) {
          System.err.println(
              "Throwing out dimension because language " +
              "dimension (" + language + ", " + languageDimension +
              ") it is not in the " + language + " vector model.");
          validDimension = false;
          break;
        }
      }

      if (validDimension) {
        validDimensions.add(multilingualDimension);
      }
    }
    System.err.println("We are left with a number of " +
                       validDimensions.size() +
                       " dimensions which are found in all vector models.");
    this.dimensionModel.dimensions = validDimensions;

    // Compute the most frequent dimensions for each POS, for each language.
    System.err.println("Computing the most frequent dimensions for " +
                       "each language...");
    TreeMap<String, Set<String>> acceptedDimensions =
        new TreeMap<String, Set<String>>();
    for (String language : this.vectorModels.keySet()) {
      Vector<String> frequentVerbLexems = this.vectorModels.get(language)
          .getMostFrequentKeys(numberOfTopmostFrequentDimensions / 2, "--V");
      Vector<String> frequentNounLexems = this.vectorModels.get(language)
          .getMostFrequentKeys(numberOfTopmostFrequentDimensions / 2, "--N");
      TreeSet<String> frequentLexems = new TreeSet<String>();
      frequentLexems.addAll(new TreeSet<String>(frequentVerbLexems));
      frequentLexems.addAll(new TreeSet<String>(frequentNounLexems));
      acceptedDimensions.put(language, frequentLexems);
    }
    System.err.println("\tDone!");

    // Purge the dimension set.
    System.err.println("Retaining the correct dimensions...");
    this.dimensionModel.purgeDimensions(acceptedDimensions);
    System.err.println("\tDone!");

    // Set the dimensions for each vector model.
    for (String language : this.vectorModels.keySet()) {
      this.vectorModels.get(language).setDimensions(
          this.dimensionModel.getDimensionsForLanguage(language));
    }
  }

  public class LanguageKeyVectorSimilarityTuple
      implements Comparable<LanguageKeyVectorSimilarityTuple> {
        public String language;
        public String key;
        public Vector<Double> vector;
        public Double similarity;

        public LanguageKeyVectorSimilarityTuple(
            String language,
            String key,
            Vector<Double> vector,
            Double similarity) {
          this.language = language;
          this.key = key;
          this.vector = vector;
          this.similarity = similarity;
        }

        // By default, Java has MAX-heaps.
        public int compareTo(LanguageKeyVectorSimilarityTuple o) {
          if (this.similarity < o.similarity) {
            return -1;
          } else if (this.similarity > o.similarity) {
            return +1;
          } else {
            return 0;
          }
        }
      }

  public Double getSimilarity(
      String originalKey,
      String originalLanguage,
      String targetKey,
      String targetLanguage) throws Exception {
    Vector<Double> originalKeyVector =
        this.vectorModels.get(originalLanguage).getVector(originalKey);
    System.err.println(VectorUtils.toString("(" + originalKey + ", " +
                                            originalLanguage + ")",
                                            originalKeyVector));
    Vector<Double> targetKeyVector =
        this.vectorModels.get(targetLanguage).getVector(targetKey);
    System.err.println(VectorUtils.toString("(" + targetKey + ", " +
                                            targetLanguage + ")",
                                            targetKeyVector));
    return VectorUtils.cosineMetric(originalKeyVector, targetKeyVector);
  }

  public Vector<LanguageKeyVectorSimilarityTuple> getClosestKNeighbours(
      String originalLanguage,
      String originalKey,
      Integer k,
      String targetLanguage,
      TreeSet<String> acceptedNeighbours) throws Exception {
    // Get the vector from the original language.
    Vector<Double> keyVector =
        this.vectorModels.get(originalLanguage).getVector(originalKey);

    // Iterate through the rest of the languages and get similar vectors.
    PriorityQueue<LanguageKeyVectorSimilarityTuple> neighbours =
        new PriorityQueue<
          MultilingualVectorModel.LanguageKeyVectorSimilarityTuple>();

    for (String foreignLanguage : this.vectorModels.keySet()) {
      if (targetLanguage != null) {
        // If we are filtering by target language, only allow for that
        // target language to be considered.
        if (!foreignLanguage.equalsIgnoreCase(targetLanguage)) {
          continue;
        }
      } else {
        // If we are not filtering by target language, then filter out
        // the original language.
        if (foreignLanguage.equalsIgnoreCase(originalLanguage)) {
          continue;
        }
      }

      VectorModel vectorModel = this.vectorModels.get(foreignLanguage);
      for (String foreignKey : vectorModel.getKeys()) {
        // If the keys are POS-tagged, only skip mismatching POS tags.
        if (originalKey.contains("--") && foreignKey.contains("--")) {
          String originalPOS =
              originalKey.substring(originalKey.length() - 1);
          String foreignPOS =
              foreignKey.substring(foreignKey.length() - 1);
          if (originalPOS.equals(foreignPOS) == false) {
            continue;
          }
        }

        // Skip same key for same language.
        if (foreignLanguage.equals(originalLanguage) &&
            foreignKey.equals(originalKey)) {
          continue;
        }

        // Skip foreign key if we have a list of accepted keys that
        // doesn't include it.
        if (acceptedNeighbours != null &&
            acceptedNeighbours.contains(foreignKey) == false) {
          continue;
        }

        // Compute the foreign vector and similarity.
        Vector<Double> foreignVector =
            vectorModel.getVector(foreignKey);
        Double similarity =
            VectorUtils.cosineMetric(keyVector, foreignVector);

        // Push the neighbor candidate to the queue and bring the
        // queue down to K elements.
        neighbours.add(new LanguageKeyVectorSimilarityTuple(
                foreignLanguage, foreignKey, foreignVector, similarity));
        while (neighbours.size() > k) {
          neighbours.remove();
        }
      }
    }

    // If we haven't got K neighbors, then throw an exception.
    if (neighbours.size() < k) {
      throw new Exception("Error. We only have " + neighbours.size() +
                          " neighbours.");
    }

    return new Vector<LanguageKeyVectorSimilarityTuple>(neighbours);
  }

  /** Loads a multilingual vector model from a .vector_model file. */
  private void parseFromFile(File inputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!inputFile.getName().endsWith(".bin_multilingual_vector_model")) {
      throw new Exception(
          "Error when trying to load MultilingualVectorModel from file '" +
          inputFile.getName() +
          "'. Expected extension '.bin_multilingual_vector_model'");
    } else {
      System.err.println("Loading MultilingualVectorModel from file '"
                         + inputFile.getName() + "'...");
    }

    // Read in object as binary.
    ObjectInputStream in =
        new ObjectInputStream(
            new FileInputStream(inputFile));
    this.swapData((MultilingualVectorModel)in.readObject());
    in.close();

    System.err.println("\tDone!");
  }

  /** Saves a vector model to a .vector_model file. */
  public void writeToFile(File outputFile) throws Exception {
    // Check that the extension of the file is .occurrence
    if (!outputFile.getName().endsWith(".bin_multilingual_vector_model")) {
      throw new Exception(
          "Error when trying to write MultilingualVectorModel to file '" +
          outputFile.getName() +
          "'. Expected extension '.bin_multilingual_vector_model'");
    } else {
      System.err.println("Writing MultilingualVectorModel to file'" +
                         outputFile.getName() + "'...");
    }

    // Writing object in binary form.
    ObjectOutputStream out =
        new ObjectOutputStream(
            new FileOutputStream(outputFile));
    out.writeObject(this);
    out.close();

    System.err.println("\tDone!");
  }
}
