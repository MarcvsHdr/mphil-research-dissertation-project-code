package selpref.data;

public class GrammaticalRelation {
  public String headLemma;
  public String headPos;
  public String headExtendedPos;
  public String dependentLemma;
  public String dependentPos;
  public String dependendExtendedPos;
  public String type;

  public GrammaticalRelation(
      String headLemma,
      String headPos,
      String headExtendedPos,
      String dependentLemma,
      String dependentPos,
      String dependentExtendedPos,
      String relationType) {
    this.headLemma = headLemma;
    this.headPos = headPos;
    this.headExtendedPos = headExtendedPos;
    this.dependentLemma = dependentLemma;
    this.dependentPos = dependentPos;
    this.dependendExtendedPos = dependentExtendedPos;
    this.type = relationType;
  }
}
