package selpref.estimators;

import java.io.*;
import java.util.*;
import selpref.data.*;
import utils.GlobalParameters;

class BaseFrequencyEstimator implements PlausibilityEstimator {

  OccurrenceModel occurrenceModel;
  Boolean logTransform;

  public BaseFrequencyEstimator(OccurrenceModel occurrenceModel,
                                Boolean logTransform) {
    this.occurrenceModel = occurrenceModel;
    this.logTransform = logTransform;
  }

  public String getName() {
    return "Frequency Estimate Baseline";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        if (this.logTransform) {
          return Math.log(
              occurrenceModel.getRelativeKeyArgumentFrequency(verb, argument) *
              occurrenceModel.getRelativeKeyFrequency(verb));
        } else {
          return
              occurrenceModel.getRelativeKeyArgumentFrequency(verb, argument) *
              occurrenceModel.getRelativeKeyFrequency(verb);
        }
      }
}

class ClassicalLDAEstimator implements PlausibilityEstimator {

  TopicModel topicModel;
  Boolean logTransform;

  public ClassicalLDAEstimator(TopicModel topicModel, Boolean logTransform) {
    this.topicModel = topicModel;
    this.logTransform = logTransform;
  }

  public String getName() {
    return "Classical LDA";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        if (logTransform) {
          return Math.log(topicModel
                          .getJointProbabilityArgumentVerbClassicalLDA(
                              verb, argument));
        } else {
          return topicModel.getJointProbabilityArgumentVerbClassicalLDA(
              verb, argument);
        }
      }
}

class ClassicalLDAEstimatorWithImprovedCoverage implements PlausibilityEstimator {

  TopicModel topicModel;
  MultilingualVectorModel mvm;
  TreeSet<String> acceptedVerbNeighbours;
  TreeSet<String> acceptedArgumentNeighbours;
  String language;
  Boolean logTransform;

  public ClassicalLDAEstimatorWithImprovedCoverage(
      TopicModel topicModel,
      MultilingualVectorModel mvm,
      String language,
      Boolean logTransform) {
    this.topicModel = topicModel;
    this.mvm = mvm;
    this.acceptedVerbNeighbours = new TreeSet<String>();
    for (String acceptedVerbNeighbour : topicModel.getKnownVerbs()) {
      this.acceptedVerbNeighbours.add(acceptedVerbNeighbour + "--V");
    }
    this.acceptedArgumentNeighbours = new TreeSet<String>();
    for (String acceptedArgumentNeighbour : topicModel.getKnownArguments()) {
      this.acceptedArgumentNeighbours.add(acceptedArgumentNeighbour + "--N");
    }
    this.language = language;
    this.logTransform = logTransform;
  }

  public String getName() {
    return "Classical LDA With Smoothing for Unknown Words";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        Double plausibility = 0.0D;
        // Keep smoothing until no more exceptions are thrown because
        // of unknown verbs or unknown arguments.
        do {
          try {
            plausibility =
                topicModel.getJointProbabilityArgumentVerbClassicalLDA(
                    verb, argument);
          } catch (Exception e) {
            if (e.getMessage().contains("Unknown verb")) {
              // Exception cause by unknown verb. We should smooth with another verb.
              String smoothingVerb = mvm.getClosestKNeighbours(
                  language, verb + "--V", 1, language,
                  this.acceptedVerbNeighbours).get(0).key;
              if (smoothingVerb.length() > 3) {
                smoothingVerb = smoothingVerb.substring(
                    0, smoothingVerb.length() - 3);
              } else {
                throw new Exception("Unknown verb '" + verb +
                                    "' cannot be smoothed with '" +
                                    smoothingVerb + ".");
              }
              System.err.println("Classical LDA improved coverage: " +
                                 "Replacing unknown verb '" + verb +
                                 "' with '" + smoothingVerb + "'...");
              verb = smoothingVerb;
              continue;
            } else if (e.getMessage().contains("Unknown argument")) {
              // Exception cause by unknown argument.
              // We should smoothe with another argument.
              String smoothingArgument = mvm.getClosestKNeighbours(
                  language, argument + "--N", 1, language,
                  this.acceptedArgumentNeighbours).get(0).key;
              if (smoothingArgument.length() > 3) {
                smoothingArgument = smoothingArgument.substring(
                    0, smoothingArgument.length() - 3);
              } else {
                throw new Exception("Unknown argument '" + argument +
                                    "' cannot be smoothed with '" +
                                    smoothingArgument + ".");
              }
              System.err.println("Classical LDA improved coverage: " +
                                 "Replacing unknown argument '" +
                                 argument + "' with '" + smoothingArgument +
                                 "'...");
              argument = smoothingArgument;
              continue;
            } else {
              // There is no unknown verb or argument. Exception comes
              // from somewhere else. Throw it up the chain.
              throw e;
            }
          }
          break;
        } while (true);
        if (logTransform) {
          return Math.log(plausibility);
        } else {
          return plausibility;
        }
      }
}

class HardcodedTransferClassicalLDAEstimator implements PlausibilityEstimator {

  TopicModel topicModel;
  Boolean logTransform;
  String golden_standard_language;
  TreeMap<String, String> hardcodedTranslations;

  public HardcodedTransferClassicalLDAEstimator(
      TopicModel topicModel,
      String golden_standard_language,
      Boolean logTransform) {
    this.topicModel = topicModel;
    this.golden_standard_language = golden_standard_language;
    this.logTransform = logTransform;
    this.hardcodedTranslations = new TreeMap<String, String>();
    if (golden_standard_language.equalsIgnoreCase("de")) {
      // 10 Verbs from the DOBJ table.
      this.hardcodedTranslations.put("belohnen", "reward");
      this.hardcodedTranslations.put("bewirken", "produce");
      this.hardcodedTranslations.put("enttäuschen", "disappoint");
      this.hardcodedTranslations.put("erlegen", "kill");
      this.hardcodedTranslations.put("formieren", "form");
      this.hardcodedTranslations.put("importieren", "import");
      this.hardcodedTranslations.put("kürzen", "cut");
      this.hardcodedTranslations.put("pumpen", "pump");
      this.hardcodedTranslations.put("reinigen", "clean");
      this.hardcodedTranslations.put("schmieden", "forge");
      // 30 Arguments from the DOBJ table.
      this.hardcodedTranslations.put("Kind", "child");
      this.hardcodedTranslations.put("Kunde", "client");
      this.hardcodedTranslations.put("Anstrengung", "exertion");
      this.hardcodedTranslations.put("Veränderung", "change");
      this.hardcodedTranslations.put("Anstieg", "rise");
      this.hardcodedTranslations.put("Beispiel", "example");
      this.hardcodedTranslations.put("Erwartung", "expectation");
      this.hardcodedTranslations.put("Gast", "customer");
      this.hardcodedTranslations.put("Politiker", "politician");
      this.hardcodedTranslations.put("Tier", "animal");
      this.hardcodedTranslations.put("Jahr", "year");
      this.hardcodedTranslations.put("Gesetz", "law");
      this.hardcodedTranslations.put("Widerstand", "resistance");
      this.hardcodedTranslations.put("Truppe", "troop");
      this.hardcodedTranslations.put("Kontur", "contour");
      this.hardcodedTranslations.put("Rindfleisch", "beef");
      this.hardcodedTranslations.put("Japaner", "japanese");
      this.hardcodedTranslations.put("Milch", "milk");
      this.hardcodedTranslations.put("Subvention", "subsidy");
      this.hardcodedTranslations.put("Leistung", "power");
      this.hardcodedTranslations.put("Seite", "page");
      this.hardcodedTranslations.put("Geld", "money");
      this.hardcodedTranslations.put("Tag", "day");
      this.hardcodedTranslations.put("Leichtigkeit", "ease");
      this.hardcodedTranslations.put("Luft", "air");
      this.hardcodedTranslations.put("Tag", "day");
      this.hardcodedTranslations.put("Gehweg", "pavement");
      this.hardcodedTranslations.put("Plan", "plan");
      this.hardcodedTranslations.put("Allianz", "alliance");
      this.hardcodedTranslations.put("Instrument", "instrument");
      // 10 Verbs from the NCSUBJ table.
      this.hardcodedTranslations.put("beten", "pray");
      this.hardcodedTranslations.put("durchfallen", "fail");
      this.hardcodedTranslations.put("glitzern", "shine");
      this.hardcodedTranslations.put("herauskommen", "emerge");
      this.hardcodedTranslations.put("knurren", "snarl");
      this.hardcodedTranslations.put("musizieren", "play");
      this.hardcodedTranslations.put("protestieren", "protest");
      this.hardcodedTranslations.put("schwappen", "swash");
      this.hardcodedTranslations.put("stagnieren", "stagnate");
      this.hardcodedTranslations.put("warten", "wait");
      // 30 Arguments from the NCSUBJ table.
      this.hardcodedTranslations.put("Papst", "pope");
      this.hardcodedTranslations.put("Pfarrer", "priest");
      this.hardcodedTranslations.put("Kreuz", "cross");
      this.hardcodedTranslations.put("Antrag", "proposal");
      this.hardcodedTranslations.put("Plan", "plan");
      this.hardcodedTranslations.put("Reifeprüfung", "examination");
      this.hardcodedTranslations.put("Sonne", "sun");
      this.hardcodedTranslations.put("Ferne", "distance");
      this.hardcodedTranslations.put("Tau", "dew");
      this.hardcodedTranslations.put("Ergebnis", "finding");
      this.hardcodedTranslations.put("Buch", "book");
      this.hardcodedTranslations.put("Zeitung", "newspaper");
      this.hardcodedTranslations.put("Magen", "stomach");
      this.hardcodedTranslations.put("Mann", "man");
      this.hardcodedTranslations.put("Knabe", "lad");
      this.hardcodedTranslations.put("Jugend", "young");
      this.hardcodedTranslations.put("Musiker", "musician");
      this.hardcodedTranslations.put("Grundschule", "school");
      this.hardcodedTranslations.put("Mensch", "person");
      this.hardcodedTranslations.put("Plan", "plan");
      this.hardcodedTranslations.put("Ordnung", "order");
      this.hardcodedTranslations.put("Welle", "wave");
      this.hardcodedTranslations.put("Bier", "beer");
      this.hardcodedTranslations.put("Rock", "skirt");
      this.hardcodedTranslations.put("Umsatz", "sales");
      this.hardcodedTranslations.put("Preis", "price");
      this.hardcodedTranslations.put("Arbeitslosigkeit", "unemployment");
      this.hardcodedTranslations.put("Welt", "world");
      this.hardcodedTranslations.put("Lohn", "wage");
      this.hardcodedTranslations.put("Veröffentlichung", "release");
    } else if (golden_standard_language.equalsIgnoreCase("es")) {
      // 10 Verbs from the DOBJ table.
      this.hardcodedTranslations.put("depurar", "purify");
      this.hardcodedTranslations.put("tapar", "cover");
      this.hardcodedTranslations.put("arrendar", "lease");
      this.hardcodedTranslations.put("anotar", "log");
      this.hardcodedTranslations.put("usurpar", "usurp");
      this.hardcodedTranslations.put("recopilar", "collect");
      this.hardcodedTranslations.put("orientar", "direct");
      this.hardcodedTranslations.put("excavar", "dig");
      this.hardcodedTranslations.put("aspirar", "aspire");
      this.hardcodedTranslations.put("rectificar", "correct");
      // 30 Arguments from the DOBJ table.
      this.hardcodedTranslations.put("agua", "water");
      this.hardcodedTranslations.put("responsabilidad", "responsibility");
      this.hardcodedTranslations.put("gusto", "taste");
      this.hardcodedTranslations.put("ojo", "eye");
      this.hardcodedTranslations.put("cabello", "hair");
      this.hardcodedTranslations.put("raja", "split");
      this.hardcodedTranslations.put("servicio", "service");
      this.hardcodedTranslations.put("terreno", "ground");
      this.hardcodedTranslations.put("huerto", "orchard");
      this.hardcodedTranslations.put("premio", "prize");
      this.hardcodedTranslations.put("observación", "observation");
      this.hardcodedTranslations.put("derribo", "throw");
      this.hardcodedTranslations.put("papel", "paper");
      this.hardcodedTranslations.put("trono", "throne");
      this.hardcodedTranslations.put("jurisdicción", "jurisdiction");
      this.hardcodedTranslations.put("libro", "book");
      this.hardcodedTranslations.put("observación", "observation");
      this.hardcodedTranslations.put("folclore", "folklore");
      this.hardcodedTranslations.put("obra", "work");
      this.hardcodedTranslations.put("objetivo", "target");
      this.hardcodedTranslations.put("voto", "vote");
      this.hardcodedTranslations.put("terreno", "ground");
      this.hardcodedTranslations.put("emplazamiento", "site");
      this.hardcodedTranslations.put("tajo", "precipice");
      this.hardcodedTranslations.put("pintura", "painting");
      this.hardcodedTranslations.put("suelo", "soil");
      this.hardcodedTranslations.put("polvo", "powder");
      this.hardcodedTranslations.put("información", "information");
      this.hardcodedTranslations.put("borde", "edge");
      this.hardcodedTranslations.put("observancia", "observance");
      // 10 Verbs from the NCSUBJ table.
      this.hardcodedTranslations.put("confirmar", "confirm");
      this.hardcodedTranslations.put("viajar", "travel");
      this.hardcodedTranslations.put("gritar", "shout");
      this.hardcodedTranslations.put("amenazar", "threaten");
      this.hardcodedTranslations.put("afrontar", "face");
      this.hardcodedTranslations.put("transportar", "transport");
      this.hardcodedTranslations.put("conquistar", "conquer");
      this.hardcodedTranslations.put("evolucionar", "evolve");
      this.hardcodedTranslations.put("paralizar", "paralyze");
      this.hardcodedTranslations.put("oscilar", "oscillate");
      // 30 Arguments from the NCSUBJ table.
      this.hardcodedTranslations.put("condición", "condition");
      this.hardcodedTranslations.put("muestra", "sample");
      this.hardcodedTranslations.put("aparecido", "ghost");
      this.hardcodedTranslations.put("estudio", "study");
      this.hardcodedTranslations.put("hermano", "brother");
      this.hardcodedTranslations.put("transbordador", "ferry");
      this.hardcodedTranslations.put("mar", "sea");
      this.hardcodedTranslations.put("espectador", "spectator");
      this.hardcodedTranslations.put("tribuna", "sideline");
      this.hardcodedTranslations.put("caso", "case");
      this.hardcodedTranslations.put("progreso", "progress");
      this.hardcodedTranslations.put("lobo", "wolf");
      this.hardcodedTranslations.put("sociedad", "society");
      this.hardcodedTranslations.put("mandato", "term");
      this.hardcodedTranslations.put("riesgo", "risk");
      this.hardcodedTranslations.put("suelo", "soil");
      this.hardcodedTranslations.put("hormona", "hormone");
      this.hardcodedTranslations.put("cloro", "chlorine");
      this.hardcodedTranslations.put("equipo", "team");
      this.hardcodedTranslations.put("cubierta", "cover");
      this.hardcodedTranslations.put("tenista", "player");
      this.hardcodedTranslations.put("universidad", "university");
      this.hardcodedTranslations.put("trastorno", "disorder");
      this.hardcodedTranslations.put("impreso", "print");
      this.hardcodedTranslations.put("ciudad", "city");
      this.hardcodedTranslations.put("deuda", "debt");
      this.hardcodedTranslations.put("ira", "anger");
      this.hardcodedTranslations.put("área", "area");
      this.hardcodedTranslations.put("precio", "price");
      this.hardcodedTranslations.put("camada", "litter");
    } else if (golden_standard_language.equalsIgnoreCase("ro")) {
      // 10 Verbs from the DOBJ table.
      this.hardcodedTranslations.put("scrie", "write");
      this.hardcodedTranslations.put("traversa", "cross");
      this.hardcodedTranslations.put("lovi", "hit");
      this.hardcodedTranslations.put("închide", "close");
      this.hardcodedTranslations.put("lega", "tie");
      this.hardcodedTranslations.put("finanța", "sponsor");
      this.hardcodedTranslations.put("cuceri", "conquer");
      this.hardcodedTranslations.put("amplfica", "amplify");
      this.hardcodedTranslations.put("combate", "battle");
      this.hardcodedTranslations.put("colora", "colour");
      // 30 Arguments from the DOBJ table.
      this.hardcodedTranslations.put("indicație", "direction");
      this.hardcodedTranslations.put("articol", "article");
      this.hardcodedTranslations.put("rând", "line");
      this.hardcodedTranslations.put("stradă", "street");
      this.hardcodedTranslations.put("perioadă", "epoch");
      this.hardcodedTranslations.put("podiș", "plateau");
      this.hardcodedTranslations.put("om", "person");
      this.hardcodedTranslations.put("braț", "arm");
      this.hardcodedTranslations.put("cadru", "frame");
      this.hardcodedTranslations.put("ochi", "eye");
      this.hardcodedTranslations.put("poartă", "gate");
      this.hardcodedTranslations.put("frizerie", "salon");
      this.hardcodedTranslations.put("șnur", "string");
      this.hardcodedTranslations.put("fular", "scarf");
      this.hardcodedTranslations.put("snop", "bundle");
      this.hardcodedTranslations.put("proiect", "project");
      this.hardcodedTranslations.put("studiu", "study");
      this.hardcodedTranslations.put("timp", "time");
      this.hardcodedTranslations.put("lume", "world");
      this.hardcodedTranslations.put("problemă", "problem");
      this.hardcodedTranslations.put("virus", "virus");
      this.hardcodedTranslations.put("problemă", "problem");
      this.hardcodedTranslations.put("efect", "effect");
      this.hardcodedTranslations.put("alarmă", "alarm");
      this.hardcodedTranslations.put("discriminare", "discrimination");
      this.hardcodedTranslations.put("tendință", "tendency");
      this.hardcodedTranslations.put("impuls", "impulse");
      this.hardcodedTranslations.put("peisaj", "landscape");
      this.hardcodedTranslations.put("rebut", "scrap");
      this.hardcodedTranslations.put("papion", "tie");
      // 10 Verbs from the NCSUBJ table.
      this.hardcodedTranslations.put("scrie", "write");
      this.hardcodedTranslations.put("traversa", "cross");
      this.hardcodedTranslations.put("lovi", "hit");
      this.hardcodedTranslations.put("închide", "close");
      this.hardcodedTranslations.put("lega", "tie");
      this.hardcodedTranslations.put("finanța", "sponsor");
      this.hardcodedTranslations.put("cuceri", "conquer");
      this.hardcodedTranslations.put("amplfica", "amplify");
      this.hardcodedTranslations.put("combate", "battle");
      this.hardcodedTranslations.put("colora", "colour");
      // 30 Arguments from the NCSUBJ table.
      this.hardcodedTranslations.put("cercetător", "researcher");
      this.hardcodedTranslations.put("redactor", "editor");
      this.hardcodedTranslations.put("stilou", "pen");
      this.hardcodedTranslations.put("călător", "traveller");
      this.hardcodedTranslations.put("pacient", "patient");
      this.hardcodedTranslations.put("râu", "river");
      this.hardcodedTranslations.put("om", "person");
      this.hardcodedTranslations.put("pumn", "fist");
      this.hardcodedTranslations.put("întâmplare", "situation");
      this.hardcodedTranslations.put("paznic", "guard");
      this.hardcodedTranslations.put("magazin", "shop");
      this.hardcodedTranslations.put("cutie", "box");
      this.hardcodedTranslations.put("muncitor", "worker");
      this.hardcodedTranslations.put("tunel", "tunnel");
      this.hardcodedTranslations.put("jurământ", "vow");
      this.hardcodedTranslations.put("guven", "government");
      this.hardcodedTranslations.put("proiect", "project");
      this.hardcodedTranslations.put("inițiativă", "initiative");
      this.hardcodedTranslations.put("inamic", "enemy");
      this.hardcodedTranslations.put("sportiv", "sportsman");
      this.hardcodedTranslations.put("melodie", "song");
      this.hardcodedTranslations.put("stație", "station");
      this.hardcodedTranslations.put("condiție", "condition");
      this.hardcodedTranslations.put("comentariu", "comment");
      this.hardcodedTranslations.put("poliție", "police");
      this.hardcodedTranslations.put("doctor", "doctor");
      this.hardcodedTranslations.put("medicament", "medicine");
      this.hardcodedTranslations.put("vopsea", "paint");
      this.hardcodedTranslations.put("fiu", "son");
      this.hardcodedTranslations.put("cafea", "coffee");
    }
  }

  public String getName() {
    return "Multilingual Transfer with Hardcoded " +
        this.golden_standard_language.toUpperCase() +
        "-EN Translations Classical LDA";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        if (logTransform) {
          return Math.log(topicModel
                          .getJointProbabilityArgumentVerbClassicalLDA(
                              this.hardcodedTranslations.get(verb),
                              this.hardcodedTranslations.get(argument)));
        } else {
          return topicModel.getJointProbabilityArgumentVerbClassicalLDA(
              this.hardcodedTranslations.get(verb),
              this.hardcodedTranslations.get(argument));
        }
      }
}

class RoothLDAEstimator implements PlausibilityEstimator {

  TopicModel topicModel;
  Boolean logTransform;

  public RoothLDAEstimator(TopicModel topicModel, Boolean logTransform) {
    this.topicModel = topicModel;
    this.logTransform = logTransform;
  }

  public String getName() {
    return "Rooth LDA";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        if (logTransform) {
          return Math.log(topicModel.getJointProbabilityArgumentVerbRoothLDA(
                  verb, argument));
        } else {
          return topicModel.getJointProbabilityArgumentVerbRoothLDA(
              verb, argument);	
        }
      }
}

class SimilarityEstimator implements PlausibilityEstimator {

  OccurrenceModel verbArgumentOccurrences;
  VectorModel vectorModel;
  Boolean logTransform;
  PrintStream debug;

  public SimilarityEstimator(
      OccurrenceModel occurrenceModel,
      VectorModel vectorModel,
      Boolean logTransform) {
    this.verbArgumentOccurrences = occurrenceModel;
    this.vectorModel = vectorModel;
    this.logTransform = logTransform;
    try {
      this.debug = new PrintStream(new File(
          "/home/marcvs/Desktop/working/Dissertation/Project/Data/debug.txt"));
    } catch (Exception e) {
    }
  }

  public String getName() {
    return "Erk Similariy";
  }

  public Double assignPlausibily(String verb, String argument) 
      throws Exception {
        Double returnValue = 0.0D;
        Set<String> arguments = verbArgumentOccurrences.getArguments(verb);
        for (String seen : arguments) {
          returnValue +=
              verbArgumentOccurrences.getRelativeKeyArgumentFrequency(
                  verb, seen) *
              verbArgumentOccurrences.getRelativeKeyFrequency(verb) *
              vectorModel.getSimilarity(argument, seen);
        }
        if (logTransform) {
          return Math.log(returnValue);
        } else {
          return returnValue;
        }
      }
}

class MonolingualSmoothedTopicModelModelEstimator
implements PlausibilityEstimator {

  TopicModel topicModel;
  TreeSet<String> acceptedNeighbours;
  MultilingualVectorModel multilingualVectorModel;
  Boolean logTransform;

  // Interpolation parameters.
  String originalLanguage;
  Double[] interpolationWeights;
  String targetLanguage;

  public MonolingualSmoothedTopicModelModelEstimator(
      TopicModel topicModel,
      MultilingualVectorModel multilingualVectorModel,
      String originalLanguage,
      Double[] interpolationWeights,
      String targetLanguage,
      Boolean logTransform) {
    this.topicModel = topicModel;
    this.acceptedNeighbours = new TreeSet<String>();
    for (String acceptedNounNeighbour : topicModel.getKnownArguments()) {
      this.acceptedNeighbours.add(acceptedNounNeighbour + "--N");
    }
    this.multilingualVectorModel = multilingualVectorModel;
    this.originalLanguage = originalLanguage;
    this.interpolationWeights = interpolationWeights;
    this.targetLanguage = targetLanguage;
    this.logTransform = logTransform;
  }

  public String getName() {
    String name = "Smoothed Classical LDA" + "(";
    for (int i = 0; i < interpolationWeights.length; ++i) {
      name += (i == 0 ? "" : ", ") + interpolationWeights[i];
    }
    return name + ")";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        Double returnValue = 0.0D;

        // Check sanity of the interpolationWeights vector.
        // All numbers have to add up to 1.
        Double interpolationWeightsSum = 0.0D;
        for (int i = 0; i < interpolationWeights.length; ++i) {
          interpolationWeightsSum += interpolationWeights[i];
        }
        if (Math.abs(interpolationWeightsSum - 1.0D) > 0.01D) {
          throw new Exception ("Interpolation weights do not add up to 1.0!" +
                               " They add up to " + interpolationWeightsSum +
                               " instead.");
        }

        // Add in the original word.
        if (logTransform) {
          returnValue += interpolationWeights[0] * Math.log(
              topicModel.getJointProbabilityArgumentVerbClassicalLDA(
                  verb, argument));
        } else {
          returnValue += interpolationWeights[0] *
              topicModel.getJointProbabilityArgumentVerbClassicalLDA(
                  verb, argument);
        }


        // The Multilingual vector model discriminates Part of Speech.
        Vector<MultilingualVectorModel.LanguageKeyVectorSimilarityTuple>
            neighbours = multilingualVectorModel.getClosestKNeighbours(
                originalLanguage,
                argument + "--N",
                interpolationWeights.length - 1,
                targetLanguage,
                acceptedNeighbours);

        for (int i = 0; i < neighbours.size(); ++i) {
          String neighbourArgument =
              neighbours.get(i).key.substring(0,
                                              neighbours.get(i).key.length() -
                                              3);
          if (logTransform) {
            returnValue += interpolationWeights[i + 1] *
                Math.log(topicModel.getJointProbabilityArgumentVerbClassicalLDA(
                        verb, neighbourArgument));
          } else {
            returnValue += interpolationWeights[i + 1] *
                topicModel.getJointProbabilityArgumentVerbClassicalLDA(
                    verb, neighbourArgument);
          }
        }
        return returnValue;
      }

}

class MultilingualTopicModelTransferEstimator
implements PlausibilityEstimator {

  TopicModel targetLanguageTopicModel;
  TreeSet<String> acceptedTargetLanguageVerbNeighbours;
  TreeSet<String> acceptedTargetLanguageNounNeighbours;
  MultilingualVectorModel multilingualVectorModel;
  Boolean logTransform;

  // Interpolation parameters.
  String originalLanguage;
  String targetLanguage;

  public MultilingualTopicModelTransferEstimator(
      TopicModel targetLanguageTopicModel,
      MultilingualVectorModel multilingualVectorModel,
      Boolean logTransform,
      String originalLanguage,
      String targetLanguage) {
    this.targetLanguageTopicModel = targetLanguageTopicModel;
    this.acceptedTargetLanguageVerbNeighbours = new TreeSet<String>();
    for (String targetLanguageVerb :
         targetLanguageTopicModel.getKnownVerbs()) {
      this.acceptedTargetLanguageVerbNeighbours.add(
          targetLanguageVerb + "--V");
    }
    this.acceptedTargetLanguageNounNeighbours = new TreeSet<String>();
    for (String targetLanguageNoun :
         targetLanguageTopicModel.getKnownArguments()) {
      this.acceptedTargetLanguageNounNeighbours.add(
          targetLanguageNoun + "--N");
    }
    this.multilingualVectorModel = multilingualVectorModel;
    this.logTransform = logTransform;
    this.originalLanguage = originalLanguage;
    this.targetLanguage = targetLanguage;
  }

  public String getName() {
    return "Target Language Multilingual Classical LDA Topic Model";
  }

  public Double assignPlausibily(String verb, String argument)
      throws Exception {
        // Get best translation for verb.
        Vector<MultilingualVectorModel.LanguageKeyVectorSimilarityTuple>
            verbNeigh = multilingualVectorModel.getClosestKNeighbours(
                originalLanguage, verb + "--V", 1,
                targetLanguage,
                this.acceptedTargetLanguageVerbNeighbours);
        String translatedVerb =
            verbNeigh.get(0).key.substring(
                0, verbNeigh.get(0).key.length() - 3);

        // Get best translation for argument.
        Vector<MultilingualVectorModel.LanguageKeyVectorSimilarityTuple>
            argumentNeigh = multilingualVectorModel.getClosestKNeighbours(
                originalLanguage, argument + "--N", 1,
                targetLanguage,
                this.acceptedTargetLanguageNounNeighbours);
        String translatedArgument =
            argumentNeigh.get(0).key.substring(
                0, argumentNeigh.get(0).key.length() - 3);

        // Add in the original word.
        if (logTransform) {
          return Math.log(targetLanguageTopicModel.
                          getJointProbabilityArgumentVerbClassicalLDA(
                  translatedVerb, translatedArgument));
        } else {
          return targetLanguageTopicModel.
              getJointProbabilityArgumentVerbClassicalLDA(
              translatedVerb, translatedArgument);
        }
      }
}

public class PlausibilityEstimatorTool {

  public static void main(String[] args) throws Exception {
    GlobalParameters gparam = PlausibilityEstimatorParameters.en_ncsubj;
    try {
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Load data models.
      TopicModel goldenStandardLanguageTopicModel = 
          new TopicModel(
              new File(
                  gparam.getString("golden-standard-language-topic-model")));
      /*
      TopicModel targetLanguageTopicModel =
        new TopicModel(new File(gparam.getString(
            "target-transfer-language-topic-model")));*/
      OccurrenceModel verbArgumentOccurrences =
          new OccurrenceModel(
              new File(gparam.getString("occurrence-model")));
      VectorModel vectorModel =
          new PMISyntacticVectorModel(
              new File(gparam.getString("vector-model")));

      // Load golden standard.
      PlausibilityDataSet dataSet = new PlausibilityDataSet();
      dataSet.parseFromFile(new File(gparam.getString("golden-standard")));

      // Estimate probabilities using various methods.
      dataSet.addEstimates(
          new BaseFrequencyEstimator(
              verbArgumentOccurrences,
              gparam.getBoolean("log-transform")));
      dataSet.addEstimates(
          new SimilarityEstimator(
              verbArgumentOccurrences,
              vectorModel,
              gparam.getBoolean("log-transform")));
      dataSet.addEstimates(
          new ClassicalLDAEstimator(
              goldenStandardLanguageTopicModel,
              gparam.getBoolean("log-transform")));
      dataSet.addEstimates(
          new RoothLDAEstimator(
              goldenStandardLanguageTopicModel,
              gparam.getBoolean("log-transform")));
      /*
      {
        Double[] interpolationWeights = new Double[2];
        interpolationWeights[0] = 0.7D;
        interpolationWeights[1] = 0.3D;
        dataSet.addEstimates(
          new MonolingualSmoothedTopicModelModelEstimator(
            goldenStandardLanguageTopicModel,
            multilingualVectorModel,
            gparam.getString("golden-standard-language"),
            interpolationWeights,
            gparam.getString("golden-standard-language"),
            gparam.getBoolean("log-transform")));
      }
      dataSet.addEstimates(
          new ClassicalLDAEstimatorWithImprovedCoverage(
              goldenStandardLanguageTopicModel,
              multilingualVectorModel,
              gparam.getString("golden-standard-language"),
              gparam.getBoolean("log-transform")));
      dataSet.addEstimates(
          new HardcodedTransferClassicalLDAEstimator(
              targetLanguageTopicModel,
              gparam.getString("golden-standard-language"),
              gparam.getBoolean("log-transform")));
      dataSet.addEstimates(
          new MultilingualTopicModelTransferEstimator(
              targetLanguageTopicModel,
              multilingualVectorModel,
              gparam.getBoolean("log-transform"),
              gparam.getString("golden-standard-language"),
              gparam.getString("target-transfer-language")));
      */

      // Print results.
      dataSet.writeToCSVFile(new File(gparam.getString("csv-output")));
      dataSet.writeToCSVPrintStream(System.out);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      return;
    }
  }
}
