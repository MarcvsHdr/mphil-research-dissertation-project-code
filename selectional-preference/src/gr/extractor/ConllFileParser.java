package gr.extractor;

import java.io.*;
import selpref.data.*;

public class ConllFileParser extends CorpusFileParser {

  SentenceProcessor sentenceProcessor;

  public ConllFileParser(
      SentenceProcessor sentenceProcessor) {
    this.sentenceProcessor = sentenceProcessor;
  }

  public void parseFile(File file) throws Exception {
    // Open file.
    BufferedReader in = new BufferedReader(new FileReader(file));

    // Initialize first sentence.
    DependencyParsedSentence sentence = new DependencyParsedSentence();
    String line;
    while ((line = in.readLine()) != null) {
      if (line.startsWith("#")) {
        // Skip comment lines.
        continue;
      } else if (line.length() == 0) {
        // Finish this sentence and start a new one.
        sentenceProcessor.processSentence(sentence);
        sentence = new DependencyParsedSentence();
      } else {
        try {
          // This is where we parse a line of the AnCora corpus.
          String[] tokens = line.split("[\\t ]+");

          // The syntax of the parsed AnCora corpus is as follows:
          // WORD-INDEX _ WORD-LEMMA GEN-POS-TYPE POS-TYPE _ HEAD-INDEX GR-TYPE _ _
          if (tokens.length != 10) {
            throw new Exception("Mismatch in number of tokens." +
                                "Expected: 10. Actual: " + tokens.length);
          }

          // Add in the word and its dependency.
          Integer sentenceIndex = Integer.parseInt(tokens[0]);
          Integer headIndex = Integer.parseInt(tokens[6]);
          // Split the INFLECTION token and recover the POSTYPE.
          String extended_pos;
          if (tokens[4].startsWith("NC")) {
            extended_pos = "common";
          } else {
            extended_pos = "other";
          }
          sentence.addWord(sentenceIndex, tokens[3], extended_pos, tokens[2]);
          sentence.addRelation(headIndex, sentenceIndex, tokens[7]);
        } catch (Exception e) {
          System.err.println(e.getMessage());
          throw new Exception("Unrecognized format in input file '" +
                              file.getName() + "' on line '" + line + "'.");
        }
      }
    }
    // Count in the final sentence.
    sentenceProcessor.processSentence(sentence);
    // Close the file.
    in.close();
  }
}
