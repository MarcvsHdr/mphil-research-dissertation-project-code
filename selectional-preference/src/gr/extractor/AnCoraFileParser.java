package gr.extractor;

import java.io.*;
import selpref.data.*;

public class AnCoraFileParser extends CorpusFileParser {

  SentenceProcessor sentenceProcessor;

  public AnCoraFileParser(
      SentenceProcessor sentenceProcessor) {
    this.sentenceProcessor = sentenceProcessor;
  }

  public void parseFile(File file) throws Exception {
    // Open file.
    BufferedReader in = new BufferedReader(new FileReader(file));

    // Initialize first sentence.
    DependencyParsedSentence sentence = new DependencyParsedSentence();
    String line;
    while ((line = in.readLine()) != null) {
      if (line.startsWith("#")) {
        // Skip comment lines.
        continue;
      } else if (line.length() == 0) {
        // Finish this sentence and start a new one.
        sentenceProcessor.processSentence(sentence);
        sentence = new DependencyParsedSentence();
      } else {
        try {
          // This is where we parse a line of the AnCora corpus.
          String[] tokens = line.split("[\\t ]+");

          // The syntax of the parsed AnCora corpus is as follows:
          // WORD-INDEX _ WORD-LEMMA _ HEAD-INDEX GR-TYPE POS-TYPE INFLECTION
          if (tokens.length != 8) {
            throw new Exception("Mismatch in number of tokens." +
                                "Expected: 8. Actual: " + tokens.length);
          }

          // Add in the word and its dependency.
          Integer sentenceIndex = Integer.parseInt(tokens[0]);
          Integer headIndex = Integer.parseInt(tokens[4]);
          // Split the INFLECTION token and recover the POSTYPE.
          String postype_inflection = "";
          String[] inflection_specs = tokens[7].split("\\|");
          for (String inflection_spec : inflection_specs) {
            if (inflection_spec.startsWith("postype=")) {
              postype_inflection =
                  inflection_spec.substring("postype=".length());
            }
          }
          sentence.addWord(sentenceIndex, tokens[6],
                           postype_inflection, tokens[2]);
          sentence.addRelation(headIndex, sentenceIndex, tokens[5]);
        } catch (Exception e) {
          System.err.println(e.getMessage());
          throw new Exception("Unrecognized format in input file '" +
                              file.getName() + "' on line '" + line + "'.");
        }
      }
    }
    // Count in the final sentence.
    sentenceProcessor.processSentence(sentence);
    // Close the file.
    in.close();
  }
}
