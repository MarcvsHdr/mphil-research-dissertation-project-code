package gr.extractor.tools;

import java.io.*;
import gr.extractor.*;
import selpref.data.*;
import utils.GlobalParameters;
import utils.GlobalParameters.*;

public class ParZuTopHeadGRVectorSpaceBuilder {

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "corpus-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/wikipedia/de",
              "The top-level directory of the corpus files",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "corpus-files-extension",
              "parsed",
              "The extension for the corpus file.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "vector-model-dimensions",
              "2000",
              "The number of dimensions in the vector model.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "output-file",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wiki.vector_model",
              "The output file for the vector model.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "noun-pos",
              "N",
              "The pos tag for nouns.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "common-noun-extended-pos",
              "NN",
              "The extended pos tag for common nouns.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "dimension-poss",
              "N,V,ADJA,ADV,PREP",
              "The pos tags for the other component in the GR",
              CommandLinePresence.FORBIDDEN));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create a new relation extractor and parse the corpus with it.
      TopHeadGRVectorSpaceBuilderSentenceProcessor occurrenceModelExtractor =
          new TopHeadGRVectorSpaceBuilderSentenceProcessor(gparam);
      ParZuFileParser relationExtractor =
          new ParZuFileParser(occurrenceModelExtractor);
      CorpusExtractor corpusExtractor =
          new CorpusExtractor(relationExtractor);
      corpusExtractor.parseCorpus(
          gparam.getString("corpus-dir"),
          gparam.getString("corpus-files-extension"));

      // Create a vector model and add all of the nouns to it.
      VectorModel vectorModel = new PMISyntacticVectorModel(
          occurrenceModelExtractor.headRelationOccurrences,
          gparam.getInteger("vector-model-dimensions"));
      vectorModel.writeToFile(new File(gparam.getString("output-file")));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
