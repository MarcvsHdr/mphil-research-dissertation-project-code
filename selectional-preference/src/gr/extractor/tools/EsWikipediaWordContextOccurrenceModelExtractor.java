package gr.extractor.tools;

import gr.extractor.*;
import java.io.*;
import selpref.data.*;
import utils.*;
import utils.GlobalParameters.*;

public class EsWikipediaWordContextOccurrenceModelExtractor {

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "corpus-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/wikipedia/es",
              "The top-level directory of the corpus files",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "corpus-files-extension",
              "5000.tagged",
              "The extension for the corpus file.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "open-class-pos-regex",
              "NC|NMEA|NMON|VL.*|VC.*", // "NC|NM|NMEA|NMON|ADJ|ADV|V.*",
              "A regex to match the accepted POSs.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "end-of-sentence-pos",
              "FS",
              "The POS-tag marking the end of a sentence.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "open-class-vocabulary",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/topWikipediaSpanishOpenClass.vocabulary",
              "A vocabulary file containing the most frequent open-class words in the language.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "window-size",
              "2",
              "The number of words to the left and to the right to be counted in the context.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data",
              "The output directory for the \"_context.vector_model\" file.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create a new relation extractor and parse the corpus with it.
      ContextOccurrenceModelBuilderSentenceProcessor occurrenceModelExtractor =
          new ContextOccurrenceModelBuilderSentenceProcessor(
              new File(gparam.getString("open-class-vocabulary")),
              null, // We won't filter by POS here, because we filter during parsing.
              gparam.getInteger("window-size"));
      TaggedWikipediaFileParser fileParser =
          new TaggedWikipediaFileParser(
              occurrenceModelExtractor,
              gparam.getString("end-of-sentence-pos"),
              gparam.getString("open-class-pos-regex"));
      CorpusExtractor corpusExtractor =
          new CorpusExtractor(fileParser);
      corpusExtractor.parseCorpus(gparam.getString("corpus-dir"),
                                  gparam.getString("corpus-files-extension"));

      // Print the resulting occurrence model.
      FrequencyContextVectorModel contextVectorModel =
          new FrequencyContextVectorModel(
              occurrenceModelExtractor.wordOccurrenceModel);
      contextVectorModel.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/es_wikipedia_medium_noun_verb_context.bin_vector_model"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
