package gr.extractor.tools;

import java.io.File;
import gr.extractor.*;
import selpref.data.*;
import utils.GlobalParameters.*;
import utils.GlobalParameters;

public class ParZuDobjNcsubjOccurrenceModelExtractor
implements SentenceProcessor {

  OccurrenceModel ncsubjOccurrences;
  OccurrenceModel dobjOccurrences;

  public ParZuDobjNcsubjOccurrenceModelExtractor() {
    this.ncsubjOccurrences = new OccurrenceModel();
    this.dobjOccurrences = new OccurrenceModel();
  }

  public void processSentence(TaggedSentence sentence) throws Exception {
    if (!(sentence instanceof DependencyParsedSentence)) {
      throw new Exception("Expecting a DependencyParsedSentence.");
    }
    DependencyParsedSentence dependencyParsedSentence =
        (DependencyParsedSentence) sentence;
    dependencyParsedSentence.countRelationsToOccurrenceModel(
        ncsubjOccurrences, "V", "N", "NN", "subj", "ncsubj");
    dependencyParsedSentence.countRelationsToOccurrenceModel(
        dobjOccurrences, "V", "N", "NN", "obja", "dobj");
  }

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "corpus-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/wikipedia/de",
              "The top-level directory of the corpus files",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "corpus-files-extension",
              ".parsed",
              "The extension for the corpus file.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data",
              "The output directory for the \"_ncsubj.occurrences\" and \"_dobj.occurrences\" files.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create a new relation extractor and parse the corpus with it.
      ParZuDobjNcsubjOccurrenceModelExtractor occurrenceModelExtractor =
          new ParZuDobjNcsubjOccurrenceModelExtractor();
      ParZuFileParser relationExtractor =
          new ParZuFileParser(occurrenceModelExtractor);
      CorpusExtractor corpusExtractor =
          new CorpusExtractor(relationExtractor);
      corpusExtractor.parseCorpus(gparam.getString("corpus-dir"),
                                  gparam.getString("corpus-files-extension"));

      // Print the resulting occurrence models.
      occurrenceModelExtractor.ncsubjOccurrences.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/de_wiki_ncsubj.occurrences"));
      occurrenceModelExtractor.dobjOccurrences.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/de_wiki_dobj.occurrences"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
