package gr.extractor.tools;

import java.io.File;
import gr.extractor.*;
import selpref.data.*;
import utils.GlobalParameters;
import utils.GlobalParameters.CommandLinePresence;

public class BNCDobjNcsubjOccurrenceModelExtractor
implements SentenceProcessor {

  OccurrenceModel ncsubjOccurrences;
  OccurrenceModel dobjOccurrences;

  public BNCDobjNcsubjOccurrenceModelExtractor() {
    this.ncsubjOccurrences = new OccurrenceModel();
    this.dobjOccurrences = new OccurrenceModel();
  }

  public void processSentence(TaggedSentence sentence) throws Exception {
    if (!(sentence instanceof DependencyParsedSentence)) {
      throw new Exception("Expecting a DependencyParsedSentence.");
    }
    DependencyParsedSentence dependencyParsedSentence =
        (DependencyParsedSentence) sentence;
    dependencyParsedSentence.countRelationsToOccurrenceModel(
        ncsubjOccurrences, "VERB", "SUBST", null, "ncsubj", "ncsubj");
    dependencyParsedSentence.countRelationsToOccurrenceModel(
        dobjOccurrences, "VERB", "SUBST", null, "dobj", "dobj");
  }

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "corpus-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/bnc-xml-rasp",
              "The top-level directory of the corpus files",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "corpus-files-extension",
              "xml",
              "The extension for the corpus file.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data",
              "The output directory for the \"_ncsubj.occurrences\" and \"_dobj.occurrences\" files.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create a new relation extractor and parse the corpus with it.
      BNCDobjNcsubjOccurrenceModelExtractor occurrenceModelExtractor =
          new BNCDobjNcsubjOccurrenceModelExtractor();
      BNCFileParser relationExtractor =
          new BNCFileParser(occurrenceModelExtractor);
      CorpusExtractor corpusExtractor =
          new CorpusExtractor(relationExtractor);
      corpusExtractor.parseCorpus(
          gparam.getString("corpus-dir"),
          gparam.getString("corpus-files-extension"));

      // Print the resulting occurrence models.
      occurrenceModelExtractor.ncsubjOccurrences.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/en_ncsubj.occurrences"));
      occurrenceModelExtractor.dobjOccurrences.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/en_dobj.occurrences"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
