package gr.extractor.tools;

import java.util.*;
import java.io.*;
import selpref.data.MultilingualVectorModel;
import utils.GlobalParameters;
import utils.VectorUtils;
import utils.GlobalParameters.CommandLinePresence;

public class MultilingualVectorModelBuilder {

  public static void testSimilarityModel(
      MultilingualVectorModel mvm,
      String languageOne,
      String keyOne,
      String languageTwo,
      String keyTwo) {
    try {
      System.err.println("Similarity between (" +
                         keyOne + ", " + languageOne + ") and (" +
                         keyTwo + ", " + languageTwo + ") == " +
                         mvm.getSimilarity(keyOne, languageOne,
                                           keyTwo, languageTwo));
    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
    }
  }

  public static void testMultilingualVectorModel(
      MultilingualVectorModel mvm,
      String language,
      String key,
      Integer k) {
    try {
      System.err.println(k + "-nearest neighbours for (" +
                         key + ", " + language + ")");
      System.err.println("Worst:");
      Vector<MultilingualVectorModel.LanguageKeyVectorSimilarityTuple> results =
          mvm.getClosestKNeighbours(language, key, k, null, null);
      for (MultilingualVectorModel.LanguageKeyVectorSimilarityTuple result :
           results) {
        System.err.println(VectorUtils.toString(
                "(" + result.key + ", " + result.language + ")[" +
                result.similarity + "]",
                result.vector));
      }
      System.err.println("^ Best.");
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getMessage());
    }
  }

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "unilingual-vector-models",
              //"/home/marcvs/Desktop/working/Dissertation/Project/Data/de_open_class_context.vector_model,/home/marcvs/Desktop/working/Dissertation/Project/Data/en_open_class_context.vector_model,/home/marcvs/Desktop/working/Dissertation/Project/Data/es_open_class_context.vector_model",
              //"/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wikipedia_noun_verb_context.bin_vector_model,/home/marcvs/Desktop/working/Dissertation/Project/Data/en_bnc_noun_verb_context.bin_vector_model",
              //"/home/marcvs/Desktop/working/Dissertation/Project/Data/de_wikipedia_medium_noun_verb_context.bin_vector_model,/home/marcvs/Desktop/working/Dissertation/Project/Data/es_wikipedia_medium_noun_verb_context.bin_vector_model",
              //"/home/marcvs/Desktop/working/Dissertation/Project/Data/en_wikipedia_medium_noun_verb_context.bin_vector_model,/home/marcvs/Desktop/working/Dissertation/Project/Data/ro_wikipedia_medium_noun_verb_context.bin_vector_model",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/en_wikipedia_medium_noun_verb_context.bin_vector_model,/home/marcvs/Desktop/working/Dissertation/Project/Data/ro_wikipedia_medium_noun_verb_context.bin_vector_model",
              "The context vector models for each of the languages.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "languages",
              //"de,en,es",
              //"en,ro",
              //"de,en",
              "en,ro",
              //"de,es",
              "The languages to be aligned.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "seed-translation-pairs",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-ro_wikimedia.translations",
              "Files containing seed translation pairs to be used as initial dimensions.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "per-language-dimension-seed",
              "5000",
              "The number of dimensions to be used as seeds from each of the initial languages (will be prunned down by the number of actual distinct translation tuples).",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "output-file",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/en-ro_wikipedia_medium_noun_verb.bin_multilingual_vector_model",
              "The output for writing the multilingual vector model.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Retrieve the input unilingual vector model file mappings.
      TreeMap<String, File> vectorModelFiles =
          new TreeMap<String, File>();
      String[] languages = gparam.getString("languages").split(",");
      String[] vectorModelFileNames =
          gparam.getString("unilingual-vector-models").split(",");
      if (languages.length != vectorModelFileNames.length) {
        throw new Exception("Number of languages should match number " +
                            "of vector models in input parameters!");
      }
      for (int i = 0; i < languages.length; ++i) {
        vectorModelFiles.put(languages[i],
                             new File(vectorModelFileNames[i]));
      }

      // Build the multilingual vector model.
      MultilingualVectorModel multilingualVectorModel =
          new MultilingualVectorModel(
              vectorModelFiles,
              gparam.getInteger("per-language-dimension-seed"),
              gparam.getString("seed-translation-pairs").split(","));

      // This is our attempt at a CLI interface.
      Scanner in = new Scanner(System.in);
      String line;
      do {
        line = in.nextLine();
        String[] tokens = line.split("[ ]+");
        if (tokens[0].equalsIgnoreCase("sim")) {
          if (tokens.length != 5) {
            System.err.println("Wrong syntax!");
            continue;
          }
          testSimilarityModel(multilingualVectorModel,
                              tokens[1],
                              tokens[2],
                              tokens[3],
                              tokens[4]);
        } else if (tokens[0].equalsIgnoreCase("neigh")) {
          if (tokens.length != 4) {
            System.err.println("Wrong syntax!");
            continue;
          }
          testMultilingualVectorModel(multilingualVectorModel,
                                      tokens[1],
                                      tokens[2],
                                      Integer.parseInt(tokens[3]));
        } else if (tokens[0].equalsIgnoreCase("quit")) {
          break;
        } else if (tokens[0].equalsIgnoreCase("iterate")) {
          if (tokens.length != 4) {
            System.err.println("Wrong syntax!");
            continue;
          }
          multilingualVectorModel.iterateBilingualDimensions(
              "de", "en",
              tokens[1],
              Integer.parseInt(tokens[2]),
              Integer.parseInt(tokens[3]));
        } else if (tokens[0].equalsIgnoreCase("write")) {
          if (tokens.length != 1) {
            System.err.println("Wrong syntax!");
            continue;
          }
          multilingualVectorModel.writeToFile(
              new File(gparam.getString("output-file")));
        }
      } while (true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
