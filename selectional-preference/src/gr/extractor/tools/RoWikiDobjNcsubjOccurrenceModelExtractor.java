package gr.extractor.tools;

import gr.extractor.RoWikiFileParser;

import java.io.File;

import selpref.data.CorpusExtractor;
import selpref.data.DependencyParsedSentence;
import selpref.data.OccurrenceModel;
import selpref.data.SentenceProcessor;
import selpref.data.TaggedSentence;
import utils.GlobalParameters;
import utils.GlobalParameters.CommandLinePresence;

public class RoWikiDobjNcsubjOccurrenceModelExtractor
implements SentenceProcessor {

  OccurrenceModel ncsubjOccurrences;
  OccurrenceModel dobjOccurrences;

  public RoWikiDobjNcsubjOccurrenceModelExtractor() {
    this.ncsubjOccurrences = new OccurrenceModel();
    this.dobjOccurrences = new OccurrenceModel();
  }

  public void processSentence(TaggedSentence sentence) throws Exception {
    if (!(sentence instanceof DependencyParsedSentence)) {
      throw new Exception("Expecting a DependencyParsedSentence.");
    }
    DependencyParsedSentence dependencyParsedSentence =
        (DependencyParsedSentence) sentence;
    dependencyParsedSentence.countRelationsToOccurrenceModel(
        ncsubjOccurrences, "VERB", "NOUN", "common", "sbj.", "ncsubj");
    dependencyParsedSentence.countRelationsToOccurrenceModel(
        dobjOccurrences, "VERB", "NOUN", "common", "c.d.", "dobj");
  }

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "corpus-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/wikipedia/ro",
              "The top-level directory of the corpus files",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "corpus-files-extension",
              "parsed",
              "The extension for the corpus file.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data",
              "The output directory for the \"_ncsubj.occurrences\" and \"_dobj.occurrences\" files.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create a new dependency parsed processor, use it to define a relation
      // extractor, and then parse the corpus with that relation extractor.
      RoWikiDobjNcsubjOccurrenceModelExtractor occurrenceModelExtractor =
          new RoWikiDobjNcsubjOccurrenceModelExtractor();
      RoWikiFileParser relationExtractor =
          new RoWikiFileParser(occurrenceModelExtractor);
      CorpusExtractor corpusExtractor =
          new CorpusExtractor(relationExtractor);
      corpusExtractor.parseCorpus(
          gparam.getString("corpus-dir"),
          gparam.getString("corpus-files-extension"));

      // Print the resulting occurrence models.
      occurrenceModelExtractor.ncsubjOccurrences.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/ro_ncsubj.occurrences"));
      occurrenceModelExtractor.dobjOccurrences.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/ro_dobj.occurrences"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
