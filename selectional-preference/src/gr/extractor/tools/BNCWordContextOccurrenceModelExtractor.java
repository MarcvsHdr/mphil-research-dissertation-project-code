package gr.extractor.tools;

import gr.extractor.*;
import selpref.data.*;

import java.io.*;
import utils.*;
import utils.GlobalParameters.*;

public class BNCWordContextOccurrenceModelExtractor {

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "corpus-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/bnc-xml-rasp/A",
              "The top-level directory of the corpus files",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "corpus-files-extension",
              "xml",
              "The extension for the corpus file.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "open-class-pos",
              "VERB,SUBST", //"SUBST,VERB,ADJ,ADV",
              "The comma-separated, open-class POS tags for the corpus.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "window-size",
              "2",
              "The number of words to the left and to the right to be counted in the context.",
              CommandLinePresence.FORBIDDEN));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data",
              "The output directory for the \"_context.occurrences\" file.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create a new relation extractor and parse the corpus with it.
      ContextOccurrenceModelBuilderSentenceProcessor occurrenceModelExtractor =
          new ContextOccurrenceModelBuilderSentenceProcessor(
              null,
              gparam.getString("open-class-pos").split(","),
              gparam.getInteger("window-size"));
      BNCFileParser relationExtractor =
          new BNCFileParser(occurrenceModelExtractor);
      CorpusExtractor corpusExtractor =
          new CorpusExtractor(relationExtractor);
      corpusExtractor.parseCorpus(
          gparam.getString("corpus-dir"),
          gparam.getString("corpus-files-extension"));

      // Print the resulting occurrence model.
      FrequencyContextVectorModel contextVectorModel =
          new FrequencyContextVectorModel(
              occurrenceModelExtractor.wordOccurrenceModel);
      contextVectorModel.writeToFile(
          new File(gparam.getString("output-dir") +
                   "/en_bnc_noun_verb_context.bin_vector_model"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
