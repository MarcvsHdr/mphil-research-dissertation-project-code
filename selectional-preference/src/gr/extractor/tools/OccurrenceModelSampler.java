package gr.extractor.tools;

import java.util.*;
import java.io.*;

import selpref.data.OccurrenceModel;
import utils.GlobalParameters;
import utils.GlobalParameters.CommandLinePresence;

class VerbArgumentPair{
  public String verb;
  public String argument;

  public VerbArgumentPair(String verb, String argument) {
    this.verb = verb;
    this.argument = argument;
  }
}

public class OccurrenceModelSampler {

  public static TreeSet<Integer> measurementPoints;

  static {
    measurementPoints = new TreeSet<Integer>();
    measurementPoints.add(1000);
    measurementPoints.add(2500);
    measurementPoints.add(5000);
    measurementPoints.add(7500);
    measurementPoints.add(10000);
    measurementPoints.add(12500);
    measurementPoints.add(15000);
    measurementPoints.add(17500);
    measurementPoints.add(20000);
    measurementPoints.add(25000);
    measurementPoints.add(30000);
    measurementPoints.add(35000);
    measurementPoints.add(40000);
    measurementPoints.add(45000);
    measurementPoints.add(50000);
    measurementPoints.add(100000);
    measurementPoints.add(500000);
    measurementPoints.add(1000000);
    measurementPoints.add(1500000);
    measurementPoints.add(2000000);
    measurementPoints.add(2500000);
    measurementPoints.add(3000000);
    measurementPoints.add(3500000);
    measurementPoints.add(4000000);
    measurementPoints.add(4500000);
    measurementPoints.add(5000000);
    measurementPoints.add(5500000);
    measurementPoints.add(6000000);
    measurementPoints.add(6500000);
    measurementPoints.add(7000000);
    measurementPoints.add(7500000);
    measurementPoints.add(8000000);
    measurementPoints.add(8500000);
  }

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "input-model",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/es_wiki_dobj.occurrences",
              "Name of the input occurence model to sample.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "output-prefix",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data/sampled/es_wiki_dobj_",
              "The prefix of the output models.",
              CommandLinePresence.FORBIDDEN));
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      /* Load the occurrence model. */
      OccurrenceModel inputData = new OccurrenceModel(new File(gparam.getString("input-model")));

      /* Move all of the data into the output data model. */
      System.err.println("Compiling and shuffling the data...");
      Vector<VerbArgumentPair> data = new Vector<VerbArgumentPair>();
      for (String key : inputData.getKeys()) {
        for (String argument : inputData.getArguments(key)) {
          Integer count = inputData.getCount(key, argument);
          for (int i = 0; i < count; ++i) {
            data.add(new VerbArgumentPair(key, argument));
          }
        }
      }
      /* Shuffle the data. */
      Collections.shuffle(data);
      System.err.println("   Done!");

      /* Slowly build the output models. */
      OccurrenceModel outputData = new OccurrenceModel();
      for (int i = 0; i < data.size(); ++i) {
        outputData.addTriple(
            data.elementAt(i).verb,
            inputData.getSecondaryKey(),
            data.elementAt(i).argument);

        /* If this is one of the magical counts, then print out the model. */
        //if (measurementPoints.contains(outputData.size()) ||
        //    i == data.size() - 1) {
        if (i == 200000 - 1 || i == 300000 - 1 ||
            i == 400000 - 1 || i == 750000 - 1) {
          System.err.println("Dumping sample for output data size: " +
                             outputData.size());
          outputData.writeToFile(
              new File(gparam.getString("output-prefix") +
                       outputData.size() + ".occurrences"));
          System.err.println("   Done!");
        }
      }

      System.err.println("Finished sampling!");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
