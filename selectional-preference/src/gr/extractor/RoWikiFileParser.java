package gr.extractor;

import java.io.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

import selpref.data.*;

class ROWIKIXMLConstants {
  public final static String SENT = "S";
  public final static String WORD = "W";

  public final static Integer INVALID_SENTENCE_INDEX = 0;
}

public class RoWikiFileParser extends CorpusFileParser {

  SentenceProcessor sentenceProcessor;
  SAXParser saxParser;
  Integer totalProcessed = 0;

  private class XMLHandler extends DefaultHandler {

    SentenceProcessor sentenceProcessor;
    DependencyParsedSentence sentence;

    public XMLHandler(SentenceProcessor sentenceProcessor) {
      this.sentenceProcessor = sentenceProcessor;
      this.sentence = new DependencyParsedSentence();
    }

    public void startElement(
        String uri,
        String localName,
        String qName,
        Attributes attributes) throws SAXException {

      if (qName.equalsIgnoreCase(ROWIKIXMLConstants.SENT)) {
        // Start of new sentence.
        sentence = new DependencyParsedSentence();
        return;
      }

      if (qName.equalsIgnoreCase(BNCXMLConstants.WORD)) {
        // Store new word.
        Integer sentenceIndex;
        try {
          if (attributes.getValue("id") == null) {
            sentenceIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
          } else if (attributes.getValue("id").contains(".")) {
            sentenceIndex = Integer.parseInt(
                attributes.getValue("id").split("\\.")[1]);
          } else {
            sentenceIndex =
                Integer.parseInt(attributes.getValue("id"));
          }
        } catch (NumberFormatException e) {
          sentenceIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
        }
        String pos = attributes.getValue("POS");
        String extended_pos = attributes.getValue("Type");
        if (extended_pos == null) {
          extended_pos = "";
        }
        String lemma = attributes.getValue("LEMMA");
        if (pos != null && lemma != null) {
          sentence.addWord(
              sentenceIndex,
              pos,
              extended_pos,
              lemma);
        }

        // And the grammatical relation that comes with it.
        String rel_type = attributes.getValue("deprel");
        if (rel_type != null) {
          Integer headIndex;
          try {
            headIndex = Integer.parseInt(attributes.getValue("head"));
          } catch (Exception e) {
            headIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
          }
          sentence.addRelation(headIndex, sentenceIndex, rel_type);
        }
        return;
      }
    }

    public void endElement(
        String uri,
        String localName,
        String qName) throws SAXException {
      if (qName.equalsIgnoreCase(BNCXMLConstants.SENT)) {
        try {
          this.sentenceProcessor.processSentence(sentence);
          totalProcessed++;
          if (totalProcessed % 1000 == 0) {
            System.err.println("So far processed " + totalProcessed +
                               " sentences...");
          }
        } catch (Exception e) {
          // Should never happen, because we are feeding in a
          // dependency parsed sentence, anyway.
        }
      }
    }

  }

  public RoWikiFileParser(
      SentenceProcessor sentenceProcessor)
      throws SAXException, ParserConfigurationException {
        // initialize the sentence processor.
        this.sentenceProcessor = sentenceProcessor;

        // Create the XML parser.
        SAXParserFactory factory = SAXParserFactory.newInstance();
        saxParser = factory.newSAXParser();
      }

  public void parseFile(File file) throws Exception {
    saxParser.parse(file, new XMLHandler(this.sentenceProcessor));
  }
}
