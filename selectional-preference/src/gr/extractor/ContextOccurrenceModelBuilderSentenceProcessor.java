package gr.extractor;

import java.io.*;
import java.util.*;
import selpref.data.*;

public class ContextOccurrenceModelBuilderSentenceProcessor
implements SentenceProcessor {

  public OccurrenceModel wordOccurrenceModel;
  String[] openClassPOSs;
  Integer wordContextWindow;
  Integer processedSentences;
  TreeSet<String> vocabulary;

  private void loadVocabularyFromFile(File vocabularyFile) {
    try {
      System.err.println("Loading vocabulary from file '" +
                         vocabularyFile.getName() + "'...");

      BufferedReader in =
          new BufferedReader(new FileReader(vocabularyFile));
      vocabulary = new TreeSet<String>();
      String line;
      while ((line = in.readLine()) != null) {
        vocabulary.add(line.split("[\\t ]+")[1]);
      }
      in.close();

      System.err.println("\tDone (" + vocabulary.size() +
                         " words found)!");
    } catch (Exception e) {
      System.err.println("Error. Failed to load vocabulary. " +
                         "Considering all lexemes.");
      vocabulary = null;
    }
  }

  public ContextOccurrenceModelBuilderSentenceProcessor(
      File vocabularyFile,
      String[] openClassPOSs,
      Integer wordContextWindow) {
    if (vocabularyFile != null) {
      loadVocabularyFromFile(vocabularyFile);
    }
    this.wordOccurrenceModel = new OccurrenceModel();
    this.openClassPOSs = openClassPOSs;
    this.wordContextWindow = wordContextWindow;
    this.processedSentences = 0;
  }

  public void processSentence(TaggedSentence sentence) {
    if (this.openClassPOSs != null) {
      for (int headPos = 0;
           headPos < this.openClassPOSs.length;
           ++headPos) {
        for (int depenendtPos = 0;
             depenendtPos < this.openClassPOSs.length;
             ++depenendtPos) {
          sentence.countWordContextsToOccurrenceModel(
              wordOccurrenceModel,
              vocabulary,
              openClassPOSs[headPos],
              openClassPOSs[depenendtPos],
              wordContextWindow);				
        }
      }
    } else {
      sentence.countWordContextsToOccurrenceModel(
          wordOccurrenceModel,
          vocabulary,
          null,
          null,
          wordContextWindow);
    }
    this.processedSentences = this.processedSentences + 1;
    if (this.processedSentences % 100000 == 0) {
      System.err.println("So far processed " +
                         this.processedSentences + " sentences...");
    }
  }
}
