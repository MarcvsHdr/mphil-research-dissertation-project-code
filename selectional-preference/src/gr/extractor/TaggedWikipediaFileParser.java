package gr.extractor;

import java.io.*;
import selpref.data.*;

public class TaggedWikipediaFileParser
extends CorpusFileParser {

  SentenceProcessor sentenceProcessor;
  String endOfSentenceToken;
  String acceptedPOSRegex;

  public TaggedWikipediaFileParser(
      SentenceProcessor sentenceProcessor,
      String endOfSentenceToken,
      String acceptedPOSRegex) {
    this.sentenceProcessor = sentenceProcessor;
    this.endOfSentenceToken = endOfSentenceToken;
    this.acceptedPOSRegex = acceptedPOSRegex;
  }

  public void parseFile(File file) throws Exception {
    // Open file.
    BufferedReader in = new BufferedReader(new FileReader(file));

    // Initialize first sentence.
    TaggedSentence sentence = new TaggedSentence();
    Integer sentenceIndex = 0;
    String line;
    while ((line = in.readLine()) != null) {
      if (line.startsWith("#")) {
        // Skip comment lines.
        // There should usually be no comment lines.
        continue;
      } else {
        try {
          // This is where we parse a line of the tagged output.
          String[] tokens = line.split("[\\t ]+");

          // The syntax of a tree-tagged corpus is as follows:
          // POS-TYPE WORD-LEMMA
          if (tokens.length != 2) {
            throw new Exception("Mismatch in number of tokens." +
                                "Expected: 2. Actual: " + tokens.length);
          }

          if (tokens[0].equalsIgnoreCase(this.endOfSentenceToken)) {
            // We've reached the end of the sentence.
            sentenceProcessor.processSentence(sentence);
            sentence = new TaggedSentence();
            sentenceIndex = 0;
          } else {
            // Add in the word.
            sentenceIndex = sentenceIndex + 1;
            if (tokens[0].matches(acceptedPOSRegex)) {
              sentence.addWord(sentenceIndex, tokens[0], tokens[0], tokens[1]);
            }
          }
        } catch (Exception e) {
          System.err.println(e.getMessage());
          throw new Exception("Unrecognized format in input file '" +
                              file.getName() + "' on line '" + line + "'.");
        }
      }
    }
    // Count in the final sentence.
    sentenceProcessor.processSentence(sentence);
    // Close the file.
    in.close();
  }
}
