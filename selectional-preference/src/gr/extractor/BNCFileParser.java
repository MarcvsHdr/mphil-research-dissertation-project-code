package gr.extractor;

import java.io.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

import selpref.data.*;

class BNCXMLConstants {
  public final static String SENT = "s";
  public final static String WORD = "w";
  public final static String GR = "gr";

  public final static Integer INVALID_SENTENCE_INDEX = 0;
}

public class BNCFileParser extends CorpusFileParser {

  SentenceProcessor sentenceProcessor;
  SAXParser saxParser;

  private class XMLHandler extends DefaultHandler {

    SentenceProcessor sentenceProcessor;
    DependencyParsedSentence sentence;

    public XMLHandler(SentenceProcessor sentenceProcessor) {
      this.sentenceProcessor = sentenceProcessor;
      this.sentence = new DependencyParsedSentence();
    }

    public void startElement(
        String uri,
        String localName,
        String qName,
        Attributes attributes) throws SAXException {

      if (qName.equalsIgnoreCase(BNCXMLConstants.SENT)) {
        // Start of new sentence.
        sentence = new DependencyParsedSentence();
        return;
      }

      if (qName.equalsIgnoreCase(BNCXMLConstants.WORD)) {
        // Store new word.
        Integer sentenceIndex;
        try {
          if (attributes.getValue("n") == null) {
            sentenceIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
          } else if (attributes.getValue("n").contains(" ")) {
            sentenceIndex = Integer.parseInt(
                attributes.getValue("n").split(" ")[0]);
          } else {
            sentenceIndex =
                Integer.parseInt(attributes.getValue("n"));
          }
        } catch (NumberFormatException e) {
          sentenceIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
        }
        String pos = attributes.getValue("pos");
        String extended_pos = attributes.getValue("rpos");
        String lemma = attributes.getValue("lem");
        if (pos != null && lemma != null) {
          sentence.addWord(
              sentenceIndex,
              pos,
              extended_pos,
              lemma.toLowerCase());
        }
        return;
      }

      if (qName.equalsIgnoreCase(BNCXMLConstants.GR)) {
        // Store grammatical relation.
        String type = attributes.getValue("type");
        Integer headIndex;
        try {
          headIndex = Integer.parseInt(attributes.getValue("head"));
        } catch (Exception e) {
          headIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
        }
        Integer dependentIndex;
        if (attributes.getValue("dep") != null) {
          dependentIndex =
              Integer.parseInt(attributes.getValue("dep"));
        } else {
          dependentIndex = BNCXMLConstants.INVALID_SENTENCE_INDEX;
        }
        sentence.addRelation(headIndex, dependentIndex, type);
        return;
      }
    }

    public void endElement(
        String uri,
        String localName,
        String qName) throws SAXException {
      if (qName.equalsIgnoreCase(BNCXMLConstants.SENT)) {
        try {
          this.sentenceProcessor.processSentence(sentence);
        } catch (Exception e) {
          // Should never happen, because we are feeding in a
          // dependency parsed sentence, anyway.
        }
      }
    }

  }

  public BNCFileParser(
      SentenceProcessor sentenceProcessor)
      throws SAXException, ParserConfigurationException {
        // initialize the sentence processor.
        this.sentenceProcessor = sentenceProcessor;

        // Create the XML parser.
        SAXParserFactory factory = SAXParserFactory.newInstance();
        saxParser = factory.newSAXParser();
      }

  public void parseFile(File file) throws Exception {
    saxParser.parse(file, new XMLHandler(this.sentenceProcessor));
  }
}
