package gr.extractor;

import java.util.Vector;

import selpref.data.*;
import utils.GlobalParameters;

public class TopHeadGRVectorSpaceBuilderSentenceProcessor 
implements SentenceProcessor {

  private GlobalParameters gparam;
  public OccurrenceModel headRelationOccurrences;

  public TopHeadGRVectorSpaceBuilderSentenceProcessor(GlobalParameters gparam) {
    this.gparam = gparam;
    this.headRelationOccurrences = new OccurrenceModel();
  }

  public void processSentence(TaggedSentence sentence) throws Exception {
    if (!(sentence instanceof DependencyParsedSentence)) {
      throw new Exception("Expecting a DependencyParsedSentence.");
    }
    DependencyParsedSentence dependencyParsedSentence =
        (DependencyParsedSentence) sentence;
    Vector<GrammaticalRelation> grs =
        dependencyParsedSentence.getGrammaticalRelations();
    String nounPos;
    String commonNounExtendedPos;
    String[] depPoss;
    try {
      nounPos = gparam.getString("noun-pos");
      commonNounExtendedPos = gparam.getString("common-noun-extended-pos");
      depPoss = gparam.getString("dimension-poss").split(",");
    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      return;
    }
    for (GrammaticalRelation gr : grs) {
      // We store the occurrences of all noun-grammatical relation pairs.
      if (gr.headPos.equalsIgnoreCase(nounPos) &&
          gr.headExtendedPos.equalsIgnoreCase(commonNounExtendedPos)) {
        Boolean acceptedDepPos = false;
        for (String depPos : depPoss) {
          if (depPos.equalsIgnoreCase(gr.dependentPos)) {
            acceptedDepPos = true;
            break;
          }
        }
        if (acceptedDepPos) {
          headRelationOccurrences.addTriple(
              gr.headLemma,
              "undefined",
              gr.dependentLemma + ":" + gr.type);
        }
      }
      if (gr.dependentPos.equalsIgnoreCase(nounPos) &&
          gr.dependendExtendedPos.equalsIgnoreCase(commonNounExtendedPos)) {
        Boolean acceptedDepPos = false;
        for (String depPos : depPoss) {
          if (depPos.equalsIgnoreCase(gr.headPos)) {
            acceptedDepPos = true;
            break;
          }
        }
        if (acceptedDepPos) {
          headRelationOccurrences.addTriple(
              gr.dependentLemma,
              "undefined",
              gr.headLemma + ":-" + gr.type);
        }
      }
    }
  }
}
