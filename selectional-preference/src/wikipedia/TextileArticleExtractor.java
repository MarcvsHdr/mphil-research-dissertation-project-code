package wikipedia;

import java.io.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class TextileArticleExtractor extends DefaultHandler {

  String fileNamePrefix;
  Integer minArticleSize;
  Integer articlesProcessed = 0;
  Integer articlesKept = 0;
  private final Integer articleGroups = 5000;

  enum ParseState {
    IN_PAGE,
    IN_PAGE_TITLE,
    IN_PAGE_TEXT,
    OUTSIDE
  }

  // Default handler states.
  ParseState currentState = ParseState.OUTSIDE;
  String currentTitle;
  StringBuffer pageContents;
  StringBuffer fileOutput = new StringBuffer();

  public void startElement(
      String uri,
      String localName,
      String qName,
      Attributes attributes) throws SAXException {
    // Listening for <page> tag.
    if (qName.equalsIgnoreCase("page")) {
      if (currentState == ParseState.OUTSIDE) {
        currentState = ParseState.IN_PAGE;
        currentTitle = null;
        pageContents = new StringBuffer();
      } else {
        throw new SAXException("Found <page> inside <page> tag.");
      }
      return;
    }

    // Listening for <title> tag.
    if (qName.equalsIgnoreCase("title")) {
      if (currentState == ParseState.IN_PAGE) {
        currentState = ParseState.IN_PAGE_TITLE;
      } else {
        throw new SAXException("Found <title> not inside <page> tag.");
      }
      return;
    }

    // Listening for <text> tag.
    if (qName.equalsIgnoreCase("text")) {
      if (currentState == ParseState.IN_PAGE) {
        currentState = ParseState.IN_PAGE_TEXT;
      } else {
        throw new SAXException("Found <text> not inside <page> tag.");
      }
      return;
    }
  }

  public void endElement(
      String uri,
      String localName,
      String qName) throws SAXException {
    // Listening for </mediawiki> tag.
    if (qName.equalsIgnoreCase("mediawiki")) {
      if (this.fileOutput.length() > 0) {
        String fileName = this.fileNamePrefix + "/" +
            (articlesKept - (articlesKept % this.articleGroups) + 1) +
            "-" + articlesKept + ".textile";
        try {
          System.err.println("Writing file " + fileName + "...");
          PrintWriter out = new PrintWriter(new File(fileName));
          out.print(this.fileOutput);
          out.close();
          this.fileOutput = new StringBuffer();
        } catch (Exception e) {
          System.err.println("Could not write file for " +
                             "article '" + fileName + "'.");
        }
      }
    }

    // Listening for </page> tag.
    if (qName.equalsIgnoreCase("page")) {
      if (currentState == ParseState.IN_PAGE) {
        // If you've found a page, then print it to disk.
        if (currentTitle != null &&
            pageContents != null &&
            this.pageContents.length() >= this.minArticleSize) {
          this.articlesKept = this.articlesKept + 1;
          this.fileOutput.append(
              "\n\n" + this.currentTitle + "\n\n");
          this.fileOutput.append(this.pageContents);
          this.fileOutput.append(
              "\n##########_END_OF_ARTICLE_##########\n");
          if (articlesKept % this.articleGroups == 0) {
            String fileName = this.fileNamePrefix + "/" +
                (articlesKept - this.articleGroups + 1) + "-" +
                articlesKept + ".textile";
            try {
              System.err.println("Writing file " + fileName + "...");
              PrintWriter out = new PrintWriter(new File(fileName));
              out.print(this.fileOutput);
              out.close();
              this.fileOutput.delete(0, this.fileOutput.length());
            } catch (Exception e) {
              e.printStackTrace();
              System.err.println("Could not write file for " +
                                 "article '" + fileName + "'.");
            }
          }
        }

        // Resetting the current title, so that we don't consider it again.
        currentState = ParseState.OUTSIDE;
        currentTitle = null;
        pageContents = null;
      } else {
        throw new SAXException("Found </page> not in <page> tag.");
      }
      this.articlesProcessed = this.articlesProcessed + 1;
      if (this.articlesProcessed % 10000 == 0) {
        System.err.println("So far kept " + articlesKept + " out of " +
                           this.articlesProcessed + " parsed articles...");
      }
      return;
    }

    // Listening for </title> tag.
    if (qName.equalsIgnoreCase("title")) {
      if (currentState == ParseState.IN_PAGE_TITLE) {
        currentState = ParseState.IN_PAGE;
      } else {
        throw new SAXException("Found </title> not inside </title> tag.");
      }
      return;
    }

    // Listening for </text> tag.
    if (qName.equalsIgnoreCase("text")) {
      if (currentState == ParseState.IN_PAGE_TEXT) {
        currentState = ParseState.IN_PAGE;
      } else {
        throw new SAXException("Found </text> not inside </text> tag.");
      }
      return;
    }
  }

  public void characters(char ch[], int start, int length)
      throws SAXException {
        // Remember the page title.
        if (currentState == ParseState.IN_PAGE_TITLE) {
          String title = new String(ch, start, length);
          if (title.matches("^[\\p{L}- ]+$")) {
            currentTitle = title;
          }
          return;
        }

        // Look for translation candidates.
        if (currentState == ParseState.IN_PAGE_TEXT && currentTitle != null) {
          pageContents.append(ch, start, length);
        }
      }

  private void extractTranslationsFromWikipediaDump(File wikipediaDump)
      throws Exception {
        // Create the parser.
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        // And parse.
        saxParser.parse(wikipediaDump, this);
      }

  public TextileArticleExtractor(
      String fileNamePrefix,
      Integer minArticleSize,
      File wikipediaDump) throws Exception {
    this.fileNamePrefix = fileNamePrefix;
    this.minArticleSize = minArticleSize;
    this.extractTranslationsFromWikipediaDump(wikipediaDump);
  }
}
