package wikipedia.tools;

import java.io.*;
import java.util.*;
import utils.GlobalParameters;
import utils.GlobalParameters.CommandLinePresence;
import wikipedia.CrossLanguageLinkExtractor;

public class WikipediaTranslationPairsExtractor {

  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "wikipedia-dump-file",
              "/media/My Passport/Corpora/wikipedia/en/enwiki-latest-pages-articles.xml",
              "Path to the wikipedia dump file.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "dump-source-language",
              "en",
              "The language of the wikipedia XML dump being parsed.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "target-languages",
              "de,es,ro",
              "Languages for which translations are being sought",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Data",
              "The output directory for the \".translations\" file.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Compile the set of target languages.
      TreeSet<String> targetLanguages = new TreeSet<String>();
      String[] targetLanguagesTokens =
          gparam.getString("target-languages").split(",");
      for (int i = 0; i < targetLanguagesTokens.length; ++i) {
        targetLanguages.add(targetLanguagesTokens[i]);
      }

      // Create an extractor and parse the Wikipedia dump file.
      CrossLanguageLinkExtractor extractor =
          new CrossLanguageLinkExtractor(
              gparam.getString("dump-source-language"),
              targetLanguages,
              new File(gparam.getString("wikipedia-dump-file")));

      // Write results to file.
      extractor.writeToFile(new File(
              gparam.getString("output-dir") +
              "/" + gparam.getString("dump-source-language") +
              "_wikipedia_all.translations"));
      // For debugging purposes, also write to standard error.
      extractor.writeToPrintStream(System.err);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
