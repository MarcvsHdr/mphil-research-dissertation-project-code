package wikipedia.tools;

import java.io.*;
import wikipedia.*;
import utils.GlobalParameters;
import utils.GlobalParameters.*;

public class WikipediaTextileArticleExtractor {
  public static void main(String[] args) {
    GlobalParameters gparam = new GlobalParameters();
    try {
      // Add parameter specification.
      gparam.addParameter(gparam.new Parameter(
              "wikipedia-dump-file",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/wikipedia/en/enwiki-latest-pages-articles.xml",
              "Path to the wikipedia dump file.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "min-article-size",
              "10000",
              "Minimum size of a wikipedia article for it to be extracted.",
              CommandLinePresence.OPTIONAL));
      gparam.addParameter(gparam.new Parameter(
              "output-dir",
              "/home/marcvs/Desktop/working/Dissertation/Project/Corpora/textile-wikipedia/en",
              "The output directory where the .textile files will be deposited.",
              CommandLinePresence.OPTIONAL));
      // Parse command line arguments.
      gparam.parseCommandLineArguments(args);
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return;
    }

    try {
      // Create an extractor and parse the Wikipedia dump file.
      new TextileArticleExtractor(
          gparam.getString("output-dir"),
          gparam.getInteger("min-article-size"),
          new File(gparam.getString("wikipedia-dump-file")));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
