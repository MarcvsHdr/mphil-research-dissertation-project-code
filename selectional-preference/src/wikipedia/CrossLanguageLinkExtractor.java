package wikipedia;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import selpref.data.DependencyParsedSentence;

public class CrossLanguageLinkExtractor extends DefaultHandler {

  // Translation information.
  String sourceLanguage;
  Set<String> targetLanguages;
  // Maps from source language word to
  // (target language, target language word)
  TreeMap<String, TreeMap<String, String>> translations;
  Integer articlesProcessed = 0;

  enum ParseState {
    IN_PAGE,
    IN_PAGE_TITLE,
    IN_PAGE_TEXT,
    OUTSIDE
  }

  // Default handler states.
  ParseState currentState = ParseState.OUTSIDE;
  String currentTitle;
  TreeMap<String, String> titleTranslations;

  public void startElement(
      String uri,
      String localName,
      String qName,
      Attributes attributes) throws SAXException {
    // Listening for <page> tag.
    if (qName.equalsIgnoreCase("page")) {
      if (currentState == ParseState.OUTSIDE) {
        currentState = ParseState.IN_PAGE;
        currentTitle = null;
        titleTranslations = null;
      } else {
        throw new SAXException("Found <page> inside <page> tag.");
      }
      return;
    }

    // Listening for <title> tag.
    if (qName.equalsIgnoreCase("title")) {
      if (currentState == ParseState.IN_PAGE) {
        currentState = ParseState.IN_PAGE_TITLE;
      } else {
        throw new SAXException("Found <title> not inside <page> tag.");
      }
      return;
    }

    // Listening for <text> tag.
    if (qName.equalsIgnoreCase("text")) {
      if (currentState == ParseState.IN_PAGE) {
        currentState = ParseState.IN_PAGE_TEXT;
      } else {
        throw new SAXException("Found <text> not inside <page> tag.");
      }
      return;
    }
  }

  public void endElement(
      String uri,
      String localName,
      String qName) throws SAXException {
    // Listening for </page> tag.
    if (qName.equalsIgnoreCase("page")) {
      if (currentState == ParseState.IN_PAGE) {
        // If you've found all, then translate current title.
        if (currentTitle != null) {
          if (titleTranslations.size() == targetLanguages.size()) {
            this.translations.put(currentTitle, titleTranslations);
          }
        }

        // Resetting the current title, so that we don't consider it again.
        currentState = ParseState.OUTSIDE;
        currentTitle = null;
        titleTranslations = null;
      } else {
        throw new SAXException("Found </page> not in <page> tag.");
      }
      this.articlesProcessed = this.articlesProcessed + 1;
      if (this.articlesProcessed % 1000 == 0) {
        System.err.println("So far parsed " +
                           this.articlesProcessed + " articles...");
      }
      return;
    }

    // Listening for </title> tag.
    if (qName.equalsIgnoreCase("title")) {
      if (currentState == ParseState.IN_PAGE_TITLE) {
        currentState = ParseState.IN_PAGE;
      } else {
        throw new SAXException("Found </title> not inside </title> tag.");
      }
      return;
    }

    // Listening for </text> tag.
    if (qName.equalsIgnoreCase("text")) {
      if (currentState == ParseState.IN_PAGE_TEXT) {
        currentState = ParseState.IN_PAGE;
      } else {
        throw new SAXException("Found </text> not inside </text> tag.");
      }
      return;
    }
  }

  public void characters(char ch[], int start, int length)
      throws SAXException {
        // Remember the page title.
        if (currentState == ParseState.IN_PAGE_TITLE) {
          String title = new String(ch, start, length);
          if (title.matches(DependencyParsedSentence.lexemRegex)) {
            currentTitle = title;
            titleTranslations = new TreeMap<String, String>();
          }
          return;
        }

        // Look for translation candidates.
        if (currentState == ParseState.IN_PAGE_TEXT && currentTitle != null) {
          String contents = new String(ch, start, length);

          // Look for translations.
          for (String language : this.targetLanguages) {
            Pattern interLanguageLink = Pattern.compile(
                "\\[\\[" + language + ":([\\p{L}-]*)\\]\\]");
            Matcher matcher = interLanguageLink.matcher(contents);
            if (matcher.find()) {
              titleTranslations.put(language, matcher.group(1));
            }
          }
        }
      }

  private void extractTranslationsFromWikipediaDump(File wikipediaDump)
      throws Exception {
        // Create the parser.
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        // And parse.
        saxParser.parse(wikipediaDump, this);
      }

  public CrossLanguageLinkExtractor(
      String sourceLanguage,
      Set<String> targetLanguages,
      File wikipediaDump) throws Exception {
    this.sourceLanguage = sourceLanguage;
    this.targetLanguages = targetLanguages;
    this.translations = new TreeMap<String, TreeMap<String, String>>();
    this.extractTranslationsFromWikipediaDump(wikipediaDump);
  }

  public void writeToPrintStream(PrintStream out) {
    StringBuffer line = new StringBuffer();

    // Output Languages.
    line.append(sourceLanguage);
    for (String language : this.targetLanguages) {
      line.append(" " + language);
    }
    out.println(line);

    // Output translation triples.
    for (String word : this.translations.keySet()) {
      line = new StringBuffer();
      line.append(word.toLowerCase());
      TreeMap<String, String> translations =
          this.translations.get(word);
      for (String language : translations.keySet()) {
        line.append(" " + translations.get(language).toLowerCase());
      }
      out.println(line);
    }
  }

  public void writeToFile(File outputFile) throws Exception {
    // Check that the extension of the file is .translations
    if (!outputFile.getName().endsWith(".translations")) {
      throw new Exception(
          "Error when trying to write Translations to file '" +
          outputFile.getName() +
          "'. Expected extension '.translations'"
          );
    } else {
      System.err.println("Writing Translations to file'" +
                         outputFile.getName() + "'...");
    }

    // Try creating a PrintStream from the file and writing there.
    PrintStream out = new PrintStream(outputFile);
    writeToPrintStream(out);
    out.close();

    System.err.println("\tDone!");
  }
}
