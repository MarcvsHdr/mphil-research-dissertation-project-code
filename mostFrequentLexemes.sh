#!/bin/bash

# This script is useful for finding the most common open class vocabulary in a
# Wikipedia-parsed corpus.

#ACCEPTED_POS="^NN|^V" # English
#ACCEPTED_POS="^NN|^VV" # German
#ACCEPTED_POS="^NC|^NMEA|^NMON|^VL|^VC" # Spanish
ACCEPTED_POS="^Nc|^V" # Romanian
LIMIT=60000

if [ $# -eq 0 ]
then
  echo "Usage: $0 (<TAGGED_CORPUS_FILES>)+"
  exit 1
fi

cat $@ | grep -E ${ACCEPTED_POS} | cut -f2 | sort | uniq -c | sort -nr | head -n ${LIMIT} | sed 's/^[ ]*//'
