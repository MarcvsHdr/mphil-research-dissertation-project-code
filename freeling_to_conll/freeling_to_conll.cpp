#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
#include <string>
#include <cstdlib>

int main(int argc, char* argv[])
{
  // Check argument usage.
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0]
        << " <INPUT_FILE> <OUTPUT_FILE>" << std::endl;
    return 1;
  }

  // Open input and output files (arguments 1 and 2)
  std::ifstream in(argv[1]);
  std::ofstream out(argv[2]);

  if (!in.good()) {
    std::cerr << "Error! Failed to open input file '"
        << argv[1] << "'." << std::endl;
    return 1;
  }

  if (!out.good()) {
    std::cerr << "Error! Failed to open output file '"
        << argv[2] << "'." << std::endl;
    return 1;
  }

  // Process all the files in the files.
  unsigned int word_no = 0;
  do {
    // We've reached the end of file.
    if (in.eof()) {
      break;
    }

    // Read in the next line.
    std::string line;
    std::getline(in, line);

    if (line.length() > 0) {
      // Bump word number.
      word_no = word_no + 1;

      // Break line up into tokens.
      char* line_str = strdup(line.c_str());
      char* token = strtok(line_str, " ");
      if (token == NULL) {
        std::cerr << "Syntax error on line: '" << line << "'.";
        return 1;
      }
      std::string word_form = std::string(token);

      token = strtok(NULL, " ");
      if (token == NULL) {
        std::cerr << "Syntax error on line: '" << line << "'.";
        return 1;
      }
      std::string word_lemma = std::string(token);

      token = strtok(NULL, " ");
      if (token == NULL) {
        std::cerr << "Syntax error on line: '" << line << "'.";
        return 1;
      }
      std::string word_pos = std::string(token);

      // Output the information derived.
      out << word_no << "\t"
          << word_form << "\t"
          << word_lemma << "\t"
          << (char) tolower(word_pos[0]) << "\t"
          << word_pos << "\t"
          << "_\t_\t_\t_\t_" << std::endl;

      // Clean up memory.
      free(line_str);

      // As an expection, if you've just printed a "." and the sentence is
      // already longer than 100 words, break it.
      if (word_form == "." && word_pos == "Fp" && word_no >= 100) {
        word_no = 0;
        //std::cerr << "Artificially broken line!" << std::endl;
        out << std::endl;
      }

      // However, if the sentence is already longer than 500 words, then break
      // it anyway!
      if (word_no >= 500) {
        word_no = 0;
        out << std::endl;
      }
    } else {
      if (word_no > 0) {
        // Echo the empty line to the output, but not consecutive lines.
        out << std::endl;
      }

      // Reset word number.
      word_no = 0;
    }

  } while (true);

  // Close files and return.
  in.close();
  out.close();
  return 0;
}
