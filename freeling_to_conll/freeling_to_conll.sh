#!/bin/bash

# This script takes as input a list of files with the ".freeling.tagged"
# extension and converts them to CoNLL format. The output will be placed in
# files with the ".conll.tagged" extension.

FILE_NO=0
for i in $@
do
  # Up file number.
  FILE_NO=`expr $FILE_NO + 1`

  # Debug information about which file is about to be processed.
  #echo "Now processing file '$i' ($FILE_NO out of $#)..."

  # Check that the file extension is correct.
  case $i in
    *\.freeling\.tagged)
      OUTPUT_FILE_NAME="${i%.freeling.tagged}.conll.tagged"
      if [ ! -f ${OUTPUT_FILE_NAME} ]
      then
        echo "Converting '$i' to '$OUTPUT_FILE_NAME'..."
        ./freeling_to_conll $i $OUTPUT_FILE_NAME
      else
        echo "Skipping '$i' because '$OUTPUT_FILE_NAME' already exists..."
      fi
      ;;
    *)
      echo "Cannot process file '$i', because extension doesn't match."
      echo "   (expected extension '.freeling.tagged')"
      echo "   Skipping..."
      ;;
  esac

  echo "Done."
done
